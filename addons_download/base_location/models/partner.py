# Copyright 2016 Nicolas Bessi, Camptocamp SA
# Copyright 2018 Tecnativa - Pedro M. Baeza
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class ResPartner(models.Model):
    _inherit = 'res.partner'
    zip_id = fields.Many2one('res.better.zip', 'ZIP Location')

    @api.onchange('city_id')
    def _onchange_city_id(self):
        if not self.zip_id:
            super(ResPartner, self)._onchange_city_id()
        if self.zip_id and self.city_id != self.zip_id.city_id:
            self.zip_id = False
            self.zip = False
            self.city = False
        if self.city_id:
            return {
                'domain': {
                    'zip_id': [('city_id', '=', self.city_id.id)]
                },
            }
        return {'domain': {'zip_id': []}}

    @api.onchange('state_id')
    def _onchange_state_id(self):
        if self.zip_id and self.state_id != self.zip_id.state_id:
            self.zip_id = False
            self.zip = False
            self.city = False

    @api.onchange('country_id')
    def _onchange_country_id(self):
        res = super(ResPartner, self)._onchange_country_id()
        if self.zip_id and self.zip_id.country_id != self.country_id:
            self.zip_id = False
        return res

    @api.onchange('zip_id')
    def _onchange_zip_id(self):
        if self.zip_id:
            self.country_id = self.zip_id.country_id
            if self.country_id.enforce_cities:
                self.city_id = self.zip_id.city_id
            self.zip = self.zip_id.name
            self.state_id = self.zip_id.state_id
            self.city = self.zip_id.city

    @api.onchange('state_id')
    def onchange_state_id(self):
        if self.state_id.country_id:
            self.country_id = self.state_id.country_id.id

    @api.multi
    def write(self, vals):

        if (   ('zip' in vals)
            or ('city' in vals)
            or ('country_id' in vals)
        ):
            zip = self.zip
            if ('zip' in vals):
                zip = vals['zip']

            city = self.city
            if ('city' in vals):
                city = vals['city']

            id_country = self.country_id.id
            if ('country_id' in vals):
                id_country = vals['country_id']

            domain = []
            if (zip):
                domain.append(('name', '=', zip))
            if (city):
                domain.append(('city', '=', city))
            if (id_country):
                domain.append(('country_id', '=', id_country))
            zip_ids = self.env['res.better.zip'].search(domain)
            if ((zip_ids) and (len(zip_ids) == 1)):
                vals['zip_id'] = zip_ids[0].id
                vals['zip'] = zip_ids[0].name
                vals['city'] = zip_ids[0].city
                vals['state_id'] = zip_ids[0].state_id.id
            else:
                vals['zip_id'] = None

        res = super(ResPartner, self).write(vals)
