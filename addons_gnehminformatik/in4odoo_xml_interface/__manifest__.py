# -*- coding: utf-8 -*-
{
    'name': 'XML Interface',

    'summary': 'Import and Export Data in XML',

    'author': "Gnehm Informatik GmbH",
    'website': "http://www.gnehm-informatik.ch",

    'application': False,
    'category': 'Tools',
    'version': '0.1',

    'depends': [
        'base',
    ],

    'data': [
        'views/xml_interface.xml',
    ],
}