# -*- coding: utf-8 -*-

import base64
import io
import logging
import xml.dom.minidom

from odoo import api, fields, models
from xml.dom.minidom import getDOMImplementation, parse

_logger = logging.getLogger(__name__)


class In4OdooXMLInterfaceImport(models.TransientModel):

    _name = 'in4odoo_xml_interface.import'
    _description = 'Import'

    file = fields.Binary(string='File to import', required=True)

    @api.model
    def import_node(self, import_parent):
        for import_data in import_parent.childNodes:
            if (import_data.nodeType == 1):
                if (import_data.hasAttribute('table')):
                    table_name = import_data.getAttribute('table')

                    values = {}
                    new_id = None
                    field_objects = self.env[table_name].fields_get()
                    for import_child in import_data.childNodes:
                        field_name = import_child.nodeName
                        import_text = import_child.firstChild
                        if ((import_text) and (import_text.nodeType == 3)):
                            value = import_text.nodeValue
                            if (field_name == 'id'):
                                new_id = value
                            else:
                                for field_object in field_objects:
                                    if (field_object == field_name):
                                        field_type = field_objects[field_name]['type']
                                        if (field_type == 'many2one'):
                                            relation_name = field_objects[field_name]['relation']
                                            if (import_data.hasAttribute(field_name)):
                                                search_field = import_data.getAttribute(field_name)
                                                domain = [(search_field, '=', value)]
                                                relation_id = self.env[relation_name].search(domain, limit=1)
                                                if not relation_id:
                                                    _logger.warning('Create in %s: "%s"' % (relation_name, value))
                                                    relation_id = self.env[relation_name].create({search_field: value})
                                                if relation_id:
                                                    value = relation_id.id
                                            else:
                                                domain = [
                                                    ('model', '=', relation_name),
                                                    ('name', '=', relation_name.replace('.', '_') + '_xml_import_' + value),
                                                ]
                                                model_data_id = self.env['ir.model.data'].search(domain, limit=1)
                                                if model_data_id:
                                                    value = model_data_id.res_id

                                        values[field_name] = value

                    old_id = None
                    res_id = None
                    if (import_data.hasAttribute('id')):
                        old_id = import_data.getAttribute('id')
                        domain = [
                            ('model', '=', table_name),
                            ('name', '=', table_name.replace('.', '_') + '_xml_import_' + old_id),
                        ]
                        model_data_id = self.env['ir.model.data'].search(domain, limit=1)
                        if model_data_id:
                            res_id = model_data_id.res_id
                            if ((new_id) and (new_id != old_id)):
                                model_data_id.res_id = new_id

                    if (values):
                        if (res_id):
                            table_id = self.env[table_name].search([('id', '=', res_id)], limit=1)
                        else:
                            table_id = None

                        if (table_id):
                            table_id.write(values)
                        else:
                            table_id = self.env[table_name].create(values)
                            id = new_id or old_id
                            if (id):
                                domain = [
                                    ('model', '=', table_name),
                                    ('name', '=', table_name.replace('.', '_') + '_xml_import_' + str(id)),
                                ]
                                model_data_id = self.env['ir.model.data'].search(domain, limit=1)
                                if model_data_id:
                                    model_data_id.res_id = table_id.id
                                else:
                                    self.env['ir.model.data'].create({
                                        'model': table_name,
                                        'name': table_name.replace('.', '_') + '_xml_import_' + str(id),
                                        'res_id': table_id.id,
                                    })

                self.import_node(import_data)

    @api.multi
    def import_data(self):
        file = io.StringIO(base64.decodebytes(self.file).decode('UTF-8'))
        import_xml = parse(file)
        import_root = import_xml.documentElement

        self.import_node(import_root)


class In4OdooXMLInterfaceExportConfig(models.Model):

    _name = 'in4odoo_xml_interface.export_config'
    _description = 'Export Configuration'
    _order = 'name'

    name = fields.Char(string='Name', required=True)
    description = fields.Char(string='Description')
    schema = fields.Text(string='Schema', default='<?xml version="1.0"?>')

    @api.model
    def table_to_xml(self, xml_doc, xml_parent, object_name, object_ids, schema_parent):
        field_names = []
        field_objects = self.env[object_name].fields_get()
        field_selection = schema_parent.getAttribute('fields')
        if (field_selection == 'include'):
            for schema_child in schema_parent.childNodes:
                if (schema_child.nodeName == 'field'):
                    field_names.append(schema_child.getAttribute('name'))
        elif (field_selection == 'exclude'):
            for field_name in field_objects:
                field_names.append(field_name)
            for schema_child in schema_parent.childNodes:
                if (schema_child.nodeName == 'field'):
                    field_names.remove(schema_child.getAttribute('name'))
        if (field_names):
            if (isinstance(object_ids, list)):
                domain = [('id', 'in', object_ids)]
            else:
                domain = []
            table_ids = self.env[object_name].search(domain)
            for table_id in table_ids:
                export_table = xml_doc.createElement(object_name)
                xml_parent.appendChild(export_table)
                export_table.setAttribute('id', str(table_id.id))
                for field_name in field_names:
                    if (field_name in field_names):
                        field_type = field_objects[field_name]['type']
                        export_field = xml_doc.createElement(field_name)
                        export_table.appendChild(export_field)
                        export_field.setAttribute('type', field_type)
                        if (   (table_id[field_name])
                            or ((field_type == 'boolean') and (isinstance(table_id[field_name], bool)))
                        ):
                            if (field_type == 'one2many'):
                                relation_name = field_objects[field_name]['relation']
                                export_field.setAttribute('relation', relation_name)
                                for schema_child in schema_parent.childNodes:
                                    if (schema_child.nodeName == 'field'):
                                        ids = table_id[field_name].ids
                                        self.table_to_xml(xml_doc, export_field, relation_name, ids, schema_child)
                            elif (field_type in ['char', 'selection']):
                                export_text = xml_doc.createTextNode(table_id[field_name])
                                export_field.appendChild(export_text)
                            elif (field_type == 'integer'):
                                export_text = xml_doc.createTextNode(str(table_id[field_name]))
                                export_field.appendChild(export_text)
                            elif (field_type == 'float'):
                                export_text = xml_doc.createTextNode(str(table_id[field_name]))
                                export_field.appendChild(export_text)
                            elif (field_type == 'boolean'):
                                if (table_id[field_name]):
                                    export_text = xml_doc.createTextNode('True')
                                else:
                                    export_text = xml_doc.createTextNode('False')
                                export_field.appendChild(export_text)
                            elif (field_type == 'many2one'):
                                export_text = xml_doc.createTextNode(str(table_id[field_name].id))
                                export_field.appendChild(export_text)

    @api.multi
    def export_data(self, context=None):
        self.ensure_one()

        content = xml.dom.minidom.parseString(self.schema)
        schema_root = content.documentElement

        impl = getDOMImplementation()
        export_xml = impl.createDocument(None, schema_root.nodeName, None)
        export_root = export_xml.documentElement

        for schema_data in schema_root.childNodes:
            if (schema_data.nodeName == 'table'):
                table_name = schema_data.getAttribute('name')
                self.table_to_xml(export_xml, export_root, table_name, None, schema_data)

        file_handle = open('filename.xml', 'wb')
        file_handle.write(export_xml.toprettyxml(encoding='UTF-8'))
        file_handle.close()