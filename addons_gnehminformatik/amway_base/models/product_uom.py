# -*- coding: utf-8 -*-

from odoo import models, fields


class AmwayBaseProductUoM(models.Model):

    _inherit = 'product.uom'

    prefix = fields.Char(string='Prefix')