# -*- coding: utf-8 -*-

import logging
from odoo import fields, models, api

_logger = logging.getLogger(__name__)

class AmwayBaseResConfigSettings(models.TransientModel):

    _inherit = 'res.config.settings'

    birthdays_prev_days = fields.Integer(string='Previous Days', default=0)
    birthdays_next_days = fields.Integer(string='Next Days', default=15)

    def set_values(self):
        super(AmwayBaseResConfigSettings, self).set_values()
        set_param = self.env['ir.config_parameter'].set_param
        set_param('amway_base.birthdays_prev_days', self.birthdays_prev_days)
        set_param('amway_base.birthdays_next_days', self.birthdays_next_days)
        self.action_recalculate_partners_name()

    @api.model
    def get_values(self):
        res = super(AmwayBaseResConfigSettings, self).get_values()
        get_param = self.env['ir.config_parameter'].sudo().get_param
        res.update(
            birthdays_prev_days=int(get_param('amway_base.birthdays_prev_days', default="0")),
            birthdays_next_days=int(get_param('amway_base.birthdays_next_days', default="15")),
        )
        return res

    @api.multi
    def partners_for_recalculating(self):
        return self.env['res.partner'].search([
            ('is_company', '=', False),
            ('date_of_birth', '!=', False),
        ])

    @api.multi
    def action_recalculate_partners_name(self):
        partners = self.partners_for_recalculating()
        _logger.info("Recalculating names for %d partners.", len(partners))
        partners.compute_birthday()
        _logger.info("%d partners updated.", len(partners))
        return True