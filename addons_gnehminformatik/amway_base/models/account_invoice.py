# -*- coding: utf-8 -*-

from odoo import models, fields, api


class AmwayBaseAccountInvoice(models.Model):

    _inherit = 'account.invoice'

    def _default_hint(self):
        invoice_type = self.env.context.get('type', 'out_invoice')
        if invoice_type == 'out_invoice':
            return self.env.user.company_id.sale_hint

    pricelist_id = fields.Many2one('product.pricelist', string='Pricelist', readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, help="Pricelist for current invoice.")
    hint = fields.Html(string='Hint', default=_default_hint)

    @api.onchange('partner_id', 'company_id')
    def _onchange_partner_id_set_pricelist_id(self):
        pricelist_id = False

        company_id = self.company_id.id
        partner_id = self.partner_id if not company_id else self.partner_id.with_context(force_company=company_id)
        if partner_id:
            type = self.type
            if type in ('out_invoice', 'out_refund'):
                pricelist_id = partner_id.property_product_pricelist.id

        self.pricelist_id = pricelist_id


class AmwayBaseAccountInvoiceLine(models.Model):

    _inherit = 'account.invoice.line'

    invoice_date_invoice = fields.Date(string='Invoice Date', compute='_get_invoice_date_invoice')

    @api.one
    @api.depends('invoice_id')
    def _get_invoice_date_invoice(self):
        if (self.invoice_id):
            self.invoice_date_invoice = self.invoice_id.date_invoice

    # Analog sale/models/sale.py: "_get_display_price"
    @api.multi
    def _get_display_price(self, product):
        if self.invoice_id.pricelist_id.discount_policy == 'with_discount':
            return product.with_context(pricelist=self.invoice_id.pricelist_id.id).price
        product_context = dict(self.env.context, partner_id=self.invoice_id.partner_id.id, date=self.invoice_id.date_invoice, uom=self.uom_id.id)
        final_price, rule_id = self.invoice_id.pricelist_id.with_context(product_context).get_product_price_rule(self.product_id, self.quantity or 1.0, self.invoice_id.partner_id)
        base_price, currency_id = self.with_context(product_context)._get_real_price_currency(product, rule_id, self.quantity, self.uom_id, self.invoice_id.pricelist_id.id)
        if currency_id != self.invoice_id.pricelist_id.currency_id.id:
            base_price = self.env['res.currency'].browse(currency_id).with_context(product_context).compute(base_price, self.invoice_id.pricelist_id.currency_id)
        # negative discounts (= surcharge) are included in the display price
        return max(base_price, final_price)

    # Analog sale/models/sale.py: "onchange('product_id')"
    @api.multi
    @api.onchange('product_id')
    def product_id_change(self):
        if not self.product_id:
            return {'domain': {'uom_id': []}}

        vals = {}
        domain = {'uom_id': [('category_id', '=', self.product_id.uom_id.category_id.id)]}
        if not self.uom_id or (self.product_id.uom_id.id != self.uom_id.id):
            vals['uom_id'] = self.product_id.uom_id
            vals['quantity'] = 1.0

        product = self.product_id.with_context(
            lang=self.invoice_id.partner_id.lang,
            partner=self.invoice_id.partner_id.id,
            quantity=vals.get('quantity') or self.quantity,
            date=self.invoice_id.date_invoice,
            pricelist=self.invoice_id.pricelist_id.id,
            uom=self.uom_id.id
        )

        result = {'domain': domain}

        if self.invoice_id.pricelist_id and self.invoice_id.partner_id:
            vals['price_unit'] = self._get_display_price(product)
        self.update(vals)

        return result

    # Analog sale/models/sale.py: "onchange('product_uom', 'product_uom_qty')"
    @api.onchange('uom_id', 'quantity')
    def product_uom_change(self):
        if not self.uom_id or not self.product_id:
            self.price_unit = 0.0
            return
        if self.invoice_id.pricelist_id and self.invoice_id.partner_id:
            product = self.product_id.with_context(
                lang=self.invoice_id.partner_id.lang,
                partner=self.invoice_id.partner_id.id,
                quantity=self.quantity,
                date=self.invoice_id.date_invoice,
                pricelist=self.invoice_id.pricelist_id.id,
                uom=self.uom_id.id,
                fiscal_position=self.env.context.get('fiscal_position')
            )
            self.price_unit = self._get_display_price(product)

    # Analog sale/models/sale.py: "_get_real_price_currency"
    @api.multi
    def _get_real_price_currency(self, product, rule_id, qty, uom, pricelist_id):
        """Retrieve the price before applying the pricelist
            :param obj product: object of current product record
            :parem float qty: total quentity of product
            :param tuple price_and_rule: tuple(price, suitable_rule) coming from pricelist computation
            :param obj uom: unit of measure of current order line
            :param integer pricelist_id: pricelist id of sales order"""
        PricelistItem = self.env['product.pricelist.item']
        #field_name = 'lst_price'
        field_name = 'list_price'
        currency_id = None
        product_currency = None
        if rule_id:
            pricelist_item = PricelistItem.browse(rule_id)
            if pricelist_item.pricelist_id.discount_policy == 'without_discount':
                while pricelist_item.base == 'pricelist' and pricelist_item.base_pricelist_id and pricelist_item.base_pricelist_id.discount_policy == 'without_discount':
                    price, rule_id = pricelist_item.base_pricelist_id.with_context(uom=uom.id).get_product_price_rule(product, qty, self.invoice_id.partner_id)
                    pricelist_item = PricelistItem.browse(rule_id)

            if pricelist_item.base == 'standard_price':
                field_name = 'standard_price'
            if pricelist_item.base == 'pricelist' and pricelist_item.base_pricelist_id:
                field_name = 'price'
                product = product.with_context(pricelist=pricelist_item.base_pricelist_id.id)
                product_currency = pricelist_item.base_pricelist_id.currency_id
            currency_id = pricelist_item.pricelist_id.currency_id

        product_currency = product_currency or (product.company_id and product.company_id.currency_id) or self.env.user.company_id.currency_id
        if not currency_id:
            currency_id = product_currency
            cur_factor = 1.0
        else:
            if currency_id.id == product_currency.id:
                cur_factor = 1.0
            else:
                cur_factor = currency_id._get_conversion_rate(product_currency, currency_id)

        uom_id = product.uom_id.id
        if uom and uom.id != uom_id:
            # the unit price is in a different uom
            uom_factor = uom._compute_price(1.0, product.uom_id)
        else:
            uom_factor = 1.0

        return product[field_name] / uom_factor * cur_factor, currency_id.id

    # Analog sale/models/sale.py: "_onchange_discount"
    @api.onchange('product_id', 'price_unit', 'uom_id', 'quantity')
    def _onchange_product_id_set_discount(self):
        if not (self.product_id and self.uom_id and
                self.invoice_id.partner_id and self.invoice_id.pricelist_id and
                self.invoice_id.pricelist_id.discount_policy == 'without_discount' and
                self.env.user.has_group('sale.group_discount_per_so_line')):
            return

        self.discount = 0.0
        product = self.product_id.with_context(
            lang=self.invoice_id.partner_id.lang,
            partner=self.invoice_id.partner_id.id,
            quantity=self.quantity,
            date=self.invoice_id.date_invoice,
            pricelist=self.invoice_id.pricelist_id.id,
            uom =self.uom_id.id,
            fiscal_position=self.env.context.get('fiscal_position')
        )

        product_context = dict(self.env.context, partner_id=self.invoice_id.partner_id.id, date=self.invoice_id.date_invoice, uom=self.uom_id.id)

        price, rule_id = self.invoice_id.pricelist_id.with_context(product_context).get_product_price_rule(self.product_id, self.quantity or 1.0, self.invoice_id.partner_id)
        new_list_price, currency_id = self.with_context(product_context)._get_real_price_currency(product, rule_id, self.quantity, self.uom_id, self.invoice_id.pricelist_id.id)

        if new_list_price != 0:
            if self.invoice_id.pricelist_id.currency_id.id != currency_id:
                # we need new_list_price in the same currency as price, which is in the SO's pricelist's currency
                new_list_price = self.env['res.currency'].browse(currency_id).with_context(product_context).compute(new_list_price, self.invoice_id.pricelist_id.currency_id)
            discount = (new_list_price - price) / new_list_price * 100
            if discount > 0:
                self.discount = discount