# -*- coding: utf-8 -*-

from . import account_invoice
from . import res_company
from . import product_uom
from . import res_config_settings
from . import res_partner
from . import res_users