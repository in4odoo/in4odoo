# -*- coding: utf-8 -*-

from odoo import fields, models

class AmwayBaseResCompany(models.Model):

    _inherit = 'res.company'

    sale_hint = fields.Html(string='Default Hint for Invoices', translate=True)