# -*- coding: utf-8 -*-

from odoo import models, fields, api
from ast import literal_eval
from datetime import datetime
from dateutil.relativedelta import relativedelta


class AmwayBaseResPartner(models.Model):

    _inherit = 'res.partner'

    date_of_birth = fields.Date(string='Date of Birth')

    birthday_month = fields.Integer(compute='compute_birthday', store=True, string='Birthday (Month)')
    birthday_day = fields.Integer(compute='compute_birthday', store=True, string='Birthday (Day)')

    birthday_begin_from = fields.Date(compute='compute_birthday', store=True, string='Birthday from (Begin)')
    birthday_begin_to = fields.Date(compute='compute_birthday', store=True, string='Birthday to (Begin)')
    birthday_end_from = fields.Date(compute='compute_birthday', store=True, string='Birthday from (End)')
    birthday_end_to = fields.Date(compute='compute_birthday', store=True, string='Birthday to (End)')

    @api.depends('date_of_birth')
    def compute_birthday(self):
        for record in self:
            if not record.date_of_birth == False:
                birthday = datetime.strptime(record.date_of_birth, '%Y-%m-%d')

                record.birthday_month = birthday.month
                record.birthday_day = birthday.day

                get_param = self.env['ir.config_parameter'].sudo().get_param
                birthdays_prev_days = int(get_param('amway_base.birthdays_prev_days'))
                birthdays_next_days = int(get_param('amway_base.birthdays_next_days'))
                birthday_in_2000 = datetime.strptime('2000' + record.date_of_birth[4:], '%Y-%m-%d')
                birthday_from = birthday_in_2000 + relativedelta(days=birthdays_prev_days)
                birthday_to = birthday_in_2000 - relativedelta(days=birthdays_next_days)

                if birthday_from.year > 2000:
                    record.birthday_begin_from = birthday_from - relativedelta(years=1)
                    record.birthday_begin_to = birthday_to - relativedelta(years=1)
                else:
                    record.birthday_begin_from = birthday_from
                    record.birthday_begin_to = birthday_to

                if birthday_to.year < 2000:
                    record.birthday_end_from = birthday_from + relativedelta(years=1)
                    record.birthday_end_to = birthday_to + relativedelta(years=1)
                else:
                    record.birthday_end_from = birthday_from
                    record.birthday_end_to = birthday_to

    @api.multi
    def action_view_partner_invoice_line(self):
        self.ensure_one()
        action = self.env.ref('amway_base.action_invoice_line_refund_out_tree').read()[0]
        action['domain'] = literal_eval(action['domain'])
        action['domain'].append(('partner_id', 'child_of', self.id))
        return action