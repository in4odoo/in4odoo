# -*- coding: utf-8 -*-
{
    'name': 'Amway (Basis)',

    'summary': """
        Basis-Erweiterung für Amway-Vertriebspartner
    """,

    'description': """
        Beinhaltet alle benötigten Erweiterungen für Amway-Vertriebspartner
    """,

    'author': 'Gnehm Informatik GmbH',
    'website': 'https://www.gnehm-informatik.ch',

    'application': True,
    'category': 'Specific Industry Applications',
    'version': '0.1',

    'depends': [
        'base',
        'contacts',
        'in4tools_backup2sftp',
        'l10n_ch_zip',
        'partner_firstname',
        'sale_management',
    ],

    'data': [
        'views/account_invoice.xml',
        'views/product_uom.xml',
        'views/res_company.xml',
        'views/res_config_settings.xml',
        'views/res_partner.xml',
        'views/res_users.xml',
        #'security/ir.model.access.csv',
    ],

    'demo': [
        'demo/demo.xml',
    ],
}