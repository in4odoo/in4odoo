# -*- coding: utf-8 -*-

import logging
import os
import socket
import time

from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo import service, tools
from odoo.exceptions import Warning

_logger = logging.getLogger(__name__)

try:
    from xmlrpc import client as xmlrpclib
except ImportError:
    import xmlrpclib

try:
    import paramiko
except ImportError:
    raise ImportError(
        'This module needs paramiko to automatically write backups to the FTP through SFTP.\n'
        'Please install paramiko on your system. (sudo pip3 install paramiko)'
    )


def execute(connector, method, *args):
    try:
        res = getattr(connector, method)(*args)
    except socket.error as error:
        _logger.critical('Error while executing the method "execute". Error: ' + str(error))
        raise error
    return res


class In4ToolsBackup2SFTPConfig(models.Model):

    _name = 'backup2sftp.config'

    host = fields.Char(string='Host', required=True, default='localhost')
    port = fields.Char(string='Port', required=True, default=7073)
    name = fields.Char(string='Database', required=True, default=lambda self: self._get_db_name())
    folder = fields.Char(string='Backup Directory', required='True', default=lambda self: self._default_folder())
    backup_type = fields.Selection([('zip', 'Zip'), ('dump', 'Dump')], string='Type', required=True, default='zip')
    auto_remove = fields.Boolean(string='Remove Backups')
    days_to_keep = fields.Integer(string='Days to keep', default=1)

    sftp_write = fields.Boolean(string='Write to SFTP')
    sftp_path = fields.Char(string='Path', default=lambda self: self._default_sftp_path())
    sftp_host = fields.Char(string='IP Address')
    sftp_port = fields.Integer(string='Port', default=22)
    sftp_user = fields.Char(string='Username')
    sftp_password = fields.Char(string='Password')
    days_to_keep_sftp = fields.Integer(string='Days to keep', default=30)
    send_mail_sftp_fail = fields.Boolean(string='Send Email on fail')
    email_to_notify = fields.Char(string='Send Email to')

    @api.multi
    def _get_db_name(self):
        return self._cr.dbname

    @api.model
    def _default_folder(self):
        return os.path.join(tools.config['data_dir'], 'backups', self.env.cr.dbname)

    @api.model
    def _default_sftp_path(self):
        return '/' + self._cr.dbname

    @api.multi
    def _check_db_exist(self):
        self.ensure_one()
        return True

    _constraints = [(_check_db_exist, _('Error: No such database exists!'), [])]

    @api.multi
    def test_sftp_connection(self, context=None):
        self.ensure_one()
        client = None
        try:
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(self.sftp_host, self.sftp_port, self.sftp_user, self.sftp_password, timeout=10)
            client.open_sftp()
            msg_title = _('Connection Test Succeeded!')
            msg_content = _('Everything seems properly set up for SFTP Backups.')
        except Exception as error:
            msg_title = _('Connection Test Failed!')
            msg_content = _('Here is what we got instead:\n%s') % (str(error))
            _logger.critical('There was a problem connecting to the remote SFTP! Error: %s' % (str(error)))
        finally:
            if client:
                client.close()
        raise Warning(msg_title + '\n\n' + msg_content)

    @api.model
    def schedule_backup(self):
        conf_ids = self.search([])
        for rec in conf_ids:
            try:
                if not os.path.isdir(rec.folder):
                    os.makedirs(rec.folder)
            except:
                raise

            file_name = '%s_%s.%s' % (time.strftime('%Y_%m_%d_%H_%M_%S'), rec.name, rec.backup_type)
            file_path = os.path.join(rec.folder, file_name)
            try:
                fp = open(file_path, 'wb')
                can_list_dbs = tools.config['list_db']
                if not can_list_dbs:
                    tools.config['list_db'] = True
                try:
                    service.db.dump_db(rec.name, fp, rec.backup_type)
                finally:
                    if not can_list_dbs:
                        tools.config['list_db'] = False
                fp.close()
            except Exception as error:
                _logger.critical(
                    'Backup of Database %s running at http://%s:%s failed! Error: %s' % (
                        rec.name,
                        rec.host,
                        rec.port,
                        str(error),
                    )
                )
                continue

            sftp = None
            if rec.sftp_write is True:
                try:
                    client = paramiko.SSHClient()
                    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                    client.connect(rec.sftp_host, rec.sftp_port, rec.sftp_user, rec.sftp_password, timeout=20)
                    sftp = client.open_sftp()
                except Exception as error:
                    _logger.critical('Connecting to SFTP Server failed! Error: ' + str(error))

            if sftp:
                try:
                    sftp.chdir(rec.sftp_path)
                except IOError:
                    path = ''
                    for dirElement in rec.sftp_path.split('/'):
                        path += dirElement + '/'
                        try:
                            sftp.chdir(path)
                        except:
                            _logger.info('Full path not exists. Create it now: ' + path)
                            sftp.mkdir(path)
                            sftp.chdir(path)
                            pass
                try:
                    for file in os.listdir(rec.folder):
                        sftp.chdir(rec.sftp_path)
                        if rec.name in file:
                            full_path = os.path.join(rec.folder, file)
                            if os.path.isfile(full_path):
                                try:
                                    sftp.stat(os.path.join(rec.sftp_path, file))
                                    _logger.critical('Copy file %s to SFTP Server failed! Error: File already exists.' % full_path)
                                except IOError:
                                    try:
                                        sftp.put(full_path, os.path.join(rec.sftp_path, file))
                                        _logger.info('File %s successfully copied.' % full_path)
                                    except Exception as err:
                                        _logger.critical('Write file to the SFTP Server failed! Error: ' + str(err))

                                    timestamp = os.stat(full_path).st_ctime
                                    file_datetime = datetime.fromtimestamp(timestamp)
                                    curr_year = file_datetime.year
                                    curr_month = file_datetime.month
                                    curr_day = file_datetime.day

                                    month_str = str(curr_month)
                                    if curr_month < 10:
                                        month_str = '0' + month_str
                                    day_str = str(curr_day)
                                    if curr_day < 10:
                                        day_str = '0' + day_str

                                    curr_path = rec.sftp_path
                                    curr_path += '/' + str(curr_year)
                                    try:
                                        sftp.chdir(curr_path)
                                    except:
                                        sftp.mkdir(curr_path)
                                        sftp.chdir(curr_path)
                                        sftp.put(full_path, os.path.join(curr_path, file))

                                    curr_path += '_' + month_str
                                    try:
                                        sftp.chdir(curr_path)
                                    except:
                                        sftp.mkdir(curr_path)
                                        sftp.chdir(curr_path)
                                        sftp.put(full_path, os.path.join(curr_path, file))

                                    curr_path += '_' + day_str
                                    try:
                                        sftp.chdir(curr_path)
                                    except:
                                        sftp.mkdir(curr_path)
                                        sftp.chdir(curr_path)
                                        sftp.put(full_path, os.path.join(curr_path, file))

                                    prev_path = rec.sftp_path
                                    prev_path += '/' + str(curr_year - 1)
                                    prev_path += '_' + month_str
                                    try:
                                        sftp.chdir(prev_path)
                                        for old_file in sftp.listdir(prev_path):
                                            sftp.unlink(old_file)
                                        sftp.rmdir(prev_path)
                                    except:
                                        pass

                                    for i in range(1, 30):
                                        delta = i + 31
                                        prev_file_datetime = file_datetime - relativedelta(days=delta)
                                        prev_path = rec.sftp_path
                                        prev_path += '/' + str(prev_file_datetime.year)
                                        if prev_file_datetime.month < 10:
                                            prev_path += '_0' + str(prev_file_datetime.month)
                                        else:
                                            prev_path += '_' + str(prev_file_datetime.month)
                                        if prev_file_datetime.day < 10:
                                            prev_path += '_0' + str(prev_file_datetime.day)
                                        else:
                                            prev_path += '_' + str(prev_file_datetime.day)
                                        try:
                                            sftp.chdir(prev_path)
                                            for old_file in sftp.listdir(prev_path):
                                                sftp.unlink(old_file)
                                            sftp.rmdir(prev_path)
                                        except:
                                            pass

                    sftp.chdir(rec.sftp_path)
                    for file in sftp.listdir(rec.sftp_path):
                        if rec.name in file:
                            full_path = os.path.join(rec.sftp_path, file)
                            file_timestamp = sftp.stat(full_path).st_atime
                            file_datetime = datetime.fromtimestamp(file_timestamp)
                            delta_datetime = datetime.now() - file_datetime
                            if (    (('.dump' in file) or ('.zip' in file))
                                and (delta_datetime.days >= rec.days_to_keep_sftp)
                            ):
                                try:
                                    sftp.unlink(file)
                                    _logger.info('Old file %s from SFTP server deleted.' % (file))
                                except:
                                    pass

                    sftp.close()
                except Exception as error:
                    _logger.debug('Backup to SFTP Server failed! Error: %s' % str(error))
                    if rec.send_mail_sftp_fail:
                        try:
                            mail_server = self.env['ir.mail_server']
                            message = _(
                                'Dear,\n'
                                '\n'
                                'The backup for the SFTP Server %s failed.\n'
                                'Please check the following details:\n'
                                '\n'
                                'IP: %s\n'
                                'Username: %s\n'
                                'Password: %s\n'
                                '\n'
                                'Error details: %s\n'
                                '\n'
                                'With kind regards'
                            ) % (rec.host, rec.sftp_host, rec.sftp_user, rec.sftp_password, tools.ustr(error))
                            msg = mail_server.build_email(
                                'administrator@in4fix.ch',
                                [rec.email_to_notify],
                                'Backup from %s (%s) failed' % (rec.host, rec.sftp_host),
                                message
                            )
                            mail_server.send_email(self._cr, self._uid, msg)
                        except Exception:
                            pass

            if rec.auto_remove:
                for file in os.listdir(rec.folder):
                    full_path = os.path.join(rec.folder, file)
                    if rec.name in full_path:
                        file_timestamp = os.stat(full_path).st_ctime
                        file_datetime = datetime.fromtimestamp(file_timestamp)
                        delta_datetime = datetime.now() - file_datetime
                        if (    (os.path.isfile(full_path))
                            and (('.dump' in file) or ('.zip' in file))
                            and (delta_datetime.days >= rec.days_to_keep)
                        ):
                            _logger.info('Delete old file %s from local Server.' % (file))
                            os.remove(full_path)
