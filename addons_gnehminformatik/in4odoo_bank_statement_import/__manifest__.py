# -*- coding: utf-8 -*-
{
    'name': 'Import Bankkontoauszug (erweitert)',

    'summary': 'Erweiterung zum Import von Bankkontoauszügen',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'application': True,
    'category': 'Account',
    'version': '0.1',

    'depends': [
        'account',
    ],

    'data': [
        'views/account_bank_statement.xml',
        'views/account_bank_statement_import.xml',
        'views/account_journal.xml',
    ],
}