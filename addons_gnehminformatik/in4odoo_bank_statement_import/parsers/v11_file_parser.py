# -*- coding: utf-8 -*-

import logging
import time
import uuid

from odoo import fields
from .base_parser import BaseSwissParser

_logger = logging.getLogger(__name__)


class V11Parser(BaseSwissParser):

    _ftype = 'v11'

    def __init__(self, data_file):
        super(V11Parser, self).__init__(data_file)
        self.lines = data_file.splitlines()
        self.balance_end = 0.0
        self.number_transaction = 0

    def file_is_known(self):
        return (self.lines[-1][0:3] in ('999', '995'))

    def _parse_currency_code(self):
        return 'CHF'

    def _parse_stmt_balance_num_trans(self):
        total_line = self.lines[-1]
        return (float(total_line[39:51])/100), int(total_line[51:63])

    def _parse_transactions(self):
        id = 0
        transactions = []
        for line in self.lines[:-1]:
            if line[0:3] in ('999', '995'):
                self.balance_end += (float(line[39:51]) / 100)
                self.number_transaction += int(line[51:63])
            else:
                ref = line[12:39]
                amount = float(line[39:49]) / 100
                format_date = time.strftime('%Y-%m-%d', time.strptime(line[71:77], '%y%m%d'))
                cost = float(line[96:100]) / 100

                if line[2] == '5':
                    amount *= -1
                    cost *= -1

                transactions.append({
                    'name': '/',
                    'ref': ref,
                    'unique_import_id': str(uuid.uuid4()),
                    'amount': amount,
                    'date': format_date,
                })
                id += 1
        return transactions

    def validate(self):
        return (len(self.statements[0]['transactions']) == self.number_transaction)

    def _parse_statement_date(self):
        year = fields.Date.today()[:4]
        date = self.lines[0][61:65]
        fdate = year + '-' + date[0:2] + '-' + date[2:]
        return fdate

    def _parse(self):
        self.currency_code = self._parse_currency_code()
        self.balance_end, self.number_transaction = self._parse_stmt_balance_num_trans()
        statement = {
            'balance_start': 0.0,
            'date': self._parse_statement_date(),
            # --> 'attachments': [('Statement File', self.data_file.encode('base64'))],
            'transactions': self._parse_transactions(),
            'balance_end_real': self.balance_end
        }
        self.statements.append(statement)
        return self.validate()