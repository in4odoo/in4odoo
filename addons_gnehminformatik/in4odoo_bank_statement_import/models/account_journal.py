# -*- coding: utf-8 -*-

from odoo import models, fields


class In4OdooBankStatementImportAccountJournal(models.Model):

    _inherit = 'account.journal'

    enforce_sequence = fields.Boolean(string='Enforce Sequence', help='If checked, the Journal Sequence will determine the statement naming policy even if the name is already set manually or by the statement import software.')