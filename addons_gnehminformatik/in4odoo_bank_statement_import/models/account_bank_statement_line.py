# -*- coding: utf-8 -*-

from odoo import models, fields, _


class In4OdooBankStatementImportAccountBankStatementLine(models.Model):

    _inherit = 'account.bank.statement.line'

    unique_import_id = fields.Char('Import ID', readonly=True, copy=False)

    _sql_constraints = [(
        'unique_import_id',
        'unique (unique_import_id)',
        'A bank account transactions can be imported only once !',
    )]