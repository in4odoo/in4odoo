# -*- coding: utf-8 -*-

import base64
import io
import logging

from ..parsers import base_parser
from datetime import datetime
from zipfile import ZipFile, BadZipfile

from odoo import api, exceptions, models, fields, _
from odoo.exceptions import Warning as UserError

_logger = logging.getLogger(__name__)


class In4OdooBankStatementImportAccountBankStatementImport(models.TransientModel):

    _name = 'account.bank.statement.import'
    _description = 'Import Bank Statement'

    @api.model
    def _get_hide_journal_field(self):
        return True

    journal_id = fields.Many2one('account.journal', string='Journal', help='Accounting journal related to the bank statement you\'re importing. It has be be manually chosen for statement formats which doesn\'t allow automatic journal detection (QIF for example).')
    hide_journal_field = fields.Boolean(string='Hide the journal field in the view', compute='_get_hide_journal_field')
    data_file = fields.Binary('Bank Statement File', required=True, help='Get you bank statements in electronic format from your bank and select them here.')
    filename = fields.Char(string='Filename')

    @api.multi
    def import_file(self):
        self.ensure_one()
        data_file = base64.b64decode(self.data_file).decode('ascii')
        statement_ids, notifications = self.with_context(active_id=self.id, filename=self.filename)._import_file(data_file)
        action = self.env.ref('account.action_bank_reconcile_bank_statements')
        return {
            'name': action.name,
            'tag': action.tag,
            'context': {'statement_ids': statement_ids, 'notifications': notifications},
            'type': 'ir.actions.client',
        }

    @api.model
    def unzip(self, data_file):
        filename = self.env.context.get('filename')
        if ((filename) and (filename.lower().endswith('.xlsx'))):
            return [data_file]
        try:
            with ZipFile(io.StringIO(data_file), 'r') as archive:
                return [
                    archive.read(name) for name in archive.namelist()
                    if not name.endswith('/')
                    ]
        except BadZipfile:
            return [data_file]

    @api.model
    def _parse_all_files(self, data_file):
        statements = []
        parse_result = self._parse_file(data_file)
        if ((isinstance(parse_result, tuple)) and (len(parse_result) == 4)):
            parser_ftype, currency_code, account_number, new_statements = parse_result
            lang_code = self.env.context.get('lang') or 'en_US'
            lang = self.env['res.lang']
            lang_id = lang._lang_get(lang_code)
            date_format = lang_id.date_format
            current_date = datetime.today().strftime(date_format)
            for stmt_vals in new_statements:
                stmt_vals['name'] = _('Import "%s" (%s)') % (parser_ftype, current_date)
                stmt_vals['currency_code'] = currency_code
                stmt_vals['account_number'] = account_number
        else:
            new_statements = parse_result
        statements += new_statements
        return statements

    @api.model
    def _import_file(self, data_file):
        statement_ids = []
        notifications = []
        statements = self._parse_all_files(data_file)
        self._check_parsed_data(statements)
        for stmt_vals in statements:
            (statement_id, new_notifications) = self._import_statement(stmt_vals)
            if statement_id:
                statement_ids.append(statement_id)
            notifications.extend(new_notifications)
        if len(statement_ids) == 0:
            raise UserError(_('You have already imported that file.'))
        return statement_ids, notifications

    @api.model
    def _import_statement(self, stmt_vals):
        currency_code = stmt_vals.pop('currency_code')
        account_number = stmt_vals.pop('account_number')
        currency_id = self._find_currency_id(currency_code)
        bank_account_id = self._find_bank_account_id(account_number)
        if not bank_account_id and account_number:
            raise UserError(_('Can not find the account number %s.') % (account_number))
        journal_id = self._get_journal(currency_id, bank_account_id)
        if not journal_id:
            raise UserError(
                _('Can not determine journal for import for account number %s and currency %s.') % (
                    account_number,
                    currency_code,
                )
            )
        stmt_vals = self._complete_statement(stmt_vals, journal_id, account_number)
        return self._create_bank_statement(stmt_vals)

    @api.model
    def _parse_file(self, data_file):
        for parser in self._get_parsers(data_file):
            if parser.file_is_known():
                parser.parse()
                parser_ftype = parser._ftype
                currency_code = parser.get_currency()
                account_number = parser.get_account_number()
                statements = parser.get_statements()

                if not statements:
                    raise exceptions.Warning(_('Nothing to import'))
                return parser_ftype, currency_code, account_number, statements
        else:
            raise UserError(_(
                'Could not make sense of the given file.\n'
                'Did you install the module to support this type of file?'
            ))

    @api.model
    def _get_parsers(self, data_file):
        for parser_class in base_parser.BaseSwissParser.__subclasses__():
            yield parser_class(data_file)

    @api.model
    def _check_parsed_data(self, statements):
        if len(statements) == 0:
            raise UserError(_('This file doesn\'t contain any statement.'))
        for stmt_vals in statements:
            if 'transactions' in stmt_vals and stmt_vals['transactions']:
                return
        raise UserError(_('This file doesn\'t contain any transaction.'))

    @api.model
    def _find_currency_id(self, currency_code):
        if currency_code:
            currency_ids = self.env['res.currency'].search([('name', '=ilike', currency_code)])
            if currency_ids:
                return currency_ids[0].id
            else:
                raise UserError(_('Statement has invalid currency code %s') % currency_code)
        return self.env.user.company_id.currency_id.id

    @api.model
    def _find_bank_account_id(self, account_number):
        bank_account_id = None
        if account_number and len(account_number) > 4:
            bank_account_ids = self.env['res.partner.bank'].search([('acc_number', '=', account_number)], limit=1)
            if bank_account_ids:
                bank_account_id = bank_account_ids[0].id
        return bank_account_id

    @api.model
    def _get_journal(self, currency_id, bank_account_id):
        bank_model = self.env['res.partner.bank']
        journal_id = self.env.context.get('journal_id') or self.journal_id.id
        currency = self.env['res.currency'].browse(currency_id)
        if bank_account_id:
            bank_account = bank_model.browse(bank_account_id)
            if journal_id:
                if (bank_account.journal_id.id and
                        bank_account.journal_id.id != journal_id):
                    raise UserError(_('The account of this statement is linked to another journal.'))
                if not bank_account.journal_id.id:
                    bank_model.write({'journal_id': journal_id})
            else:
                if bank_account.journal_id.id:
                    journal_id = bank_account.journal_id.id
        if journal_id and currency_id:
            journal_obj = self.env['account.journal'].browse(journal_id)
            if journal_obj.currency_id:
                journal_currency_id = journal_obj.currency_id.id
                if currency_id != journal_currency_id:
                    _logger.warning('Statement currency id is %d, but journal currency id = %d.', currency_id, journal_currency_id)
                    raise UserError(
                        _('The currency of the bank statement (%s) is not the same as the currency of the journal %s (%s)!') % (
                            currency.name,
                            journal_obj.name,
                            journal_obj.currency_id.name
                        ))
            else:
                company_currency = self.env.user.company_id.currency_id
                if currency_id != company_currency.id:
                    _logger.warning('Statement currency id is %d, but company currency id = %d.', currency_id, company_currency.id)
                    raise UserError(
                        _('The currency of the bank statement (%s) is not the same as the company currency (%s)!') % (
                            currency.name,
                            company_currency.name,
                        ))
        return journal_id

    @api.model
    @api.returns('res.partner.bank')
    def _create_bank_account(self, account_number, company_id=False, currency_id=False):
        try:
            bank_type = self.env.ref('base.bank_normal')
            bank_code = bank_type.code
        except ValueError:
            bank_code = 'bank'
        vals_acc = {
            'acc_number': account_number,
            'state': bank_code,
        }
        if company_id:
            vals = self.env['res.partner.bank'].onchange_company_id(company_id)
            vals_acc.update(vals.get('value', {}))
            vals_acc['company_id'] = company_id

        return self.env['res.partner.bank'].with_context(default_currency_id=currency_id, default_currency=currency_id).create(vals_acc)

    @api.model
    def _complete_statement(self, stmt_vals, journal_id, account_number):
        stmt_vals['journal_id'] = journal_id
        for line_vals in stmt_vals['transactions']:
            unique_import_id = line_vals.get('unique_import_id', False)
            if unique_import_id:
                line_vals['unique_import_id'] = (account_number and account_number + '-' or '') + unique_import_id
            if not line_vals.get('bank_account_id'):
                partner_id = False
                bank_account_id = False
                partner_account_number = line_vals.get('account_number')
                if partner_account_number:
                    bank_model = self.env['res.partner.bank']
                    banks = bank_model.search([('acc_number', '=', partner_account_number)], limit=1)
                    if banks:
                        bank_account_id = banks[0].id
                        partner_id = banks[0].partner_id.id
                    else:
                        bank_obj = self._create_bank_account(partner_account_number)
                        bank_account_id = bank_obj and bank_obj.id or False
                line_vals['partner_id'] = partner_id
                line_vals['bank_account_id'] = bank_account_id
        return stmt_vals

    @api.model
    def _create_bank_statement(self, stmt_vals):
        bs_model = self.env['account.bank.statement']
        bsl_model = self.env['account.bank.statement.line']
        ignored_line_ids = []
        filtered_st_lines = []
        for line_vals in stmt_vals['transactions']:
            unique_id = 'unique_import_id' in line_vals and line_vals['unique_import_id']
            if (not unique_id) or (not bool(bsl_model.sudo().search([('unique_import_id', '=', unique_id)], limit=1))):
                filtered_st_lines.append(line_vals)
            else:
                ignored_line_ids.append(unique_id)
        statement_id = False
        if len(filtered_st_lines) > 0:
            stmt_vals.pop('transactions', None)
            for line_vals in filtered_st_lines:
                line_vals.pop('account_number', None)
            stmt_vals['line_ids'] = [[0, False, line] for line in filtered_st_lines]
            statement_id = bs_model.create(stmt_vals).id
        notifications = []
        num_ignored = len(ignored_line_ids)
        if num_ignored > 0:
            if num_ignored > 1:
                msg = _('%d transactions had already been imported and were ignored.') % (num_ignored)
            else:
                msg = _('One transaction had already been imported and was ignored.')
            notifications += [{
                'type': 'warning',
                'message': msg,
                'details': {
                    'name': _('Already imported items'),
                    'model': 'account.bank.statement.line',
                    'ids': bsl_model.search([('unique_import_id', 'in', ignored_line_ids)]).ids}
            }]
        return statement_id, notifications