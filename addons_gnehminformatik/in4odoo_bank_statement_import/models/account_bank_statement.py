# -*- coding: utf-8 -*-

from odoo import api, models


class In4OdooBankStatementImportAccountBankStatement(models.Model):

    _inherit = 'account.bank.statement'

    @api.model
    def create(self, vals):
        if vals.get('name'):
            journal_id = self.env['account.journal'].browse(vals.get('journal_id'))
            if journal_id.enforce_sequence:
                vals['name'] = '/'

        return super(In4OdooBankStatementImportAccountBankStatement, self).create(vals)