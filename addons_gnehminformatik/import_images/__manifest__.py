# -*- coding: utf-8 -*-
{
    'name': "Import Images",

    'summary': """
        Import Images from CSV File
    """,

    'description': """
        Import Images from CSV File (File Path/Web URL)
    """,

    'author': "Gnehm Informatik GmbH",
    'website': "http://www.gnehm-informatik.ch",

    'application': False,
    'category': 'Sales',
    'version': '0.1',

    'depends': [
    ],

    'data': [
        'views/import_images.xml',
    ],
}