# -*- coding: utf-8 -*-

import csv
import urllib
import base64
import io
import sys
from odoo import models, fields, api
from odoo.exceptions import Warning


class ImagesImportWizard(models.TransientModel):

    _name = 'import.images'

    product_model = fields.Selection([('1', 'Product Template'), ('2', 'Product Variants')], required=True, default='2', string='Product Model')
    pdt_operation = fields.Selection([('1', 'Product Creation'), ('2', 'Product Updation')], required=True, default='2', string='Product Operation')
    file = fields.Binary('File to import', required=True)

    @api.multi
    def import_file(self):
        file = io.StringIO(base64.decodebytes(self.file).decode('utf-8'))
        reader = csv.reader(file, delimiter=',')
        csv.field_size_limit(sys.maxsize)
        skip_header = True
        for row in reader:
            if skip_header:
                skip_header = False
                continue
            product = row[0]
            image_path = row[1]
            image_link = row[2]

            if self.product_model == '1':
                product_obj = self.env['product.template']
            else:
                product_obj = self.env['product.product']
            product_id = product_obj.search([('default_code', '=', product)])

            ok = False
            image_base64 = None
            if (not ok) and (('.jpg' in image_path) or ('.jpeg' in image_path)):
                try:
                    with open(image_path, 'rb') as image:
                        image_base64 = base64.b64encode(image.read())
                    ok = True
                except:
                    Warning('Please provide correct path "%s" for product "%s" or check your image size.' % (image_path, product))

            if (not ok) and (('http://' in image_link) or ('https://' in image_link)):
                try:
                    link = urllib.request.urlopen(image_link).read()
                    image_base64 = base64.encodebytes(link)
                    ok = True
                except:
                    Warning('Please provide correct URL "%s" for product "%s" or check your image size.' % (image_link, product))

            if (ok):
                vals_prepare = {
                    'image': None,
                }
                vals_load = {
                    'image': image_base64,
                }
                try:
                    if self.pdt_operation == '1' and not product_id:
                        product_obj.create(vals_load)
                    elif self.pdt_operation == '1' and product_id:
                        if self.product_model == '1':
                            product_id.write(vals_prepare)
                        else:
                            product_id.product_tmpl_id.write(vals_prepare)
                        product_id.write(vals_load)
                    elif self.pdt_operation == '2' and product_id:
                        if self.product_model == '1':
                            product_id.write(vals_prepare)
                        else:
                            product_id.product_tmpl_id.write(vals_prepare)
                        product_id.write(vals_load)
                    elif not product_id and self.pdt_operation == '2':
                        Warning('Could not find the product "%s"' % product)
                except:
                    Warning('Fehler')
