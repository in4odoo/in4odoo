# -*- coding: utf-8 -*-
{
    'name': "Google People Sync",

    'summary': """
        Google People Synchronization
    """,

    'description': """
        Google People 2-way-Synchronization

        Link to enable the People API: https://developers.google.com/people/quickstart/python
    """,

    'author': "Gnehm Informatik GmbH",
    'website': "https://www.gnehm-informatik.ch",

    'application': True,
    'category': 'Tools',
    'version': '0.1',

    'depends': [
        'base',
        'contacts',
        'partner_firstname',
    ],

    'data': [
        'views/google_people_sync.xml',
        'views/res_partner.xml',
        'security/ir.model.access.csv',
    ],
}