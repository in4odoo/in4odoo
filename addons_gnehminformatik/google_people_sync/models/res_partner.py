# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT

from .oauth2client import file
from .googleapiclient.discovery import build
from .httplib2 import Http

import time
import logging
_logger = logging.getLogger(__name__)
#_logger.info()
#_logger.warning()


delete_type = [
    ('none', 'Delete only in Odoo'),
    ('delete', 'Delete Google Contact'),
    ('remove', 'Remove from the Odoo Group / Add to the Private Group'),
]


class GooglePeopleSyncResPartnerCategory(models.Model):

    _inherit = 'res.partner.category'

    google_group = fields.Char(string='Google Group')


class GooglePeopleSyncResPartner(models.Model):

    _inherit = 'res.partner'

    google_people_sync_settings_id = fields.Many2one('google_people_sync.settings', string='Settings', copy=False)
    google_resource_name = fields.Char(string='Resource Name', copy=False)
    google_download_timestamp = fields.Datetime(string='Last Download', copy=False)

    @api.model
    def _get_extendedAddress_and_poBox(self):
        extendedAddress = ''
        poBox = ''
        if (self.street2):
            extendedAddress = self.street2
            if ((self.google_people_sync_settings_id) and (self.google_people_sync_settings_id.po_box_list)):
                street2_lower = self.street2.lower()
                po_box_lines = self.google_people_sync_settings_id.po_box_list.split('\n')
                for po_box_line in po_box_lines:
                    if (po_box_line.lower() in street2_lower):
                        poBox = self.street2
                        extendedAddress = ''
                        break

        return extendedAddress, poBox

    @api.model
    def create(self, vals):
        result = super(GooglePeopleSyncResPartner, self).create(vals)

        google_people_sync_settings_id = self.env['google_people_sync.settings'].search([], limit=1)
        if ((google_people_sync_settings_id) and (google_people_sync_settings_id.upload_selection == 'auto_upload')):
            result.upload_contact_ok(google_people_sync_settings_id)

        return result

    @api.multi
    def write(self, vals):
        result = super(GooglePeopleSyncResPartner, self).write(vals)

        try:
            #https://developers.google.com/people/v1/write-people
            if (    (self.google_people_sync_settings_id)
                and (self.google_people_sync_settings_id.upload_selection in ['auto_write', 'auto_upload'])
                and (self.google_resource_name)
                and (not 'google_download_timestamp' in vals)
                and (  ('name' in vals)
                    or ('lastname' in vals)
                    or ('firstname' in vals)
                    or ('street' in vals)
                    or ('street2' in vals)
                    or ('zip' in vals)
                    or ('city' in vals)
                    or ('country_id' in vals)
                    or ('phone' in vals)
                    or ('mobile' in vals)
                    or ('email' in vals)
                    or ('website' in vals)
                    or ('comment' in vals)
                    or ('category_id' in vals)
                    or ('parent_id' in vals)
                    or ('child_ids' in vals)
                    or ('google_people_sync_settings_id' in vals)
                    or (    (self.google_people_sync_settings_id.birthday_field)
                        and (self.google_people_sync_settings_id.birthday_field in vals)
                    )
                )
            ):
                token = self.google_people_sync_settings_id.token
                service = build('people', 'v1', http=file.Storage(token).get().authorize(Http()))

                person = service.people().get(
                    resourceName = self.google_resource_name,
                    personFields='addresses,biographies,birthdays,emailAddresses,memberships,metadata,names,organizations,phoneNumbers,urls,birthdays',
                    # personFields='addresses,ageRanges,biographies,birthdays,braggingRights,coverPhotos,emailAddresses,events,genders,imClients,interests,locales,memberships,metadata,names,nicknames,occupations,organizations,phoneNumbers,photos,relations,relationshipInterests,relationshipStatuses,residences,sipAddresses,skills,taglines,urls,userDefined',
                ).execute()
                etag = person.get('etag')

                addresses_old = person.get('addresses')
                phone_numbers_old = person.get('phoneNumbers')
                email_addresses_old = person.get('emailAddresses')
                urls_old = person.get('urls')

                memberships_old = person.get('memberships')
                for membership_old in memberships_old:
                    contactGroupId = membership_old.get('contactGroupMembership').get('contactGroupId')
                    if (contactGroupId):
                        domain = [('google_group', '=', 'contactGroups/' + contactGroupId)]
                        category_id = self.env['res.partner.category'].search(domain, limit=1)
                        if ((category_id) and (not (category_id in self.category_id))):
                            service.contactGroups().members().modify(
                                resourceName='contactGroups/' + contactGroupId,
                                body={'resourceNamesToRemove': [self.google_resource_name]},
                            ).execute()
                for category_id in self.category_id:
                    if (category_id.google_group):
                        found = False
                        for membership_old in memberships_old:
                            contactGroupId = membership_old.get('contactGroupMembership').get('contactGroupId')
                            if (category_id.google_group == 'contactGroups/' + contactGroupId):
                                found = True
                        if (not found):
                            service.contactGroups().members().modify(
                                resourceName=category_id.google_group,
                                body={'resourceNamesToAdd': [self.google_resource_name]},
                            ).execute()

                birthdays = person.get('birthdays')
                if (self.google_people_sync_settings_id.birthday_field):
                    birthday_str = self[self.google_people_sync_settings_id.birthday_field]
                    if (birthday_str):
                        birthday = datetime.strptime(birthday_str, DEFAULT_SERVER_DATE_FORMAT)
                        birthdays = [{'date': {'year': birthday.year, 'month': birthday.month, 'day': birthday.day}}]
                    else:
                        birthdays = []

                if (self.is_company):
                    if (self.name):
                        organizationName = self.name
                    else:
                        organizationName = ''

                    extendedAddress, poBox = self._get_extendedAddress_and_poBox()
                    addresses_new = [{
                        'type': 'work',
                        'streetAddress': self.street or '',
                        'extendedAddress': extendedAddress,
                        'poBox': poBox,
                        'postalCode': self.zip or '',
                        'city': self.city or '',
                        'country': self.country_id.name or '',
                        'countryCode': self.country_id.code or '',
                    }]
                    if (addresses_old):
                        for address in addresses_old:
                            address_type = address.get('type')
                            if ((not address_type) or (address_type != 'work')):
                                addresses_new.append(address)

                    phone_numbers_new = [
                        {'type': 'work', 'value': self.phone or ''},
                        {'type': 'mobile', 'value': self.mobile or ''},
                    ]
                    if (phone_numbers_old):
                        for phone_number in phone_numbers_old:
                            phone_type = phone_number.get('type')
                            if ((not phone_type) or (not phone_type in ['work', 'mobile'])):
                                phone_numbers_new.append(phone_number)

                    email_addresses_new = [{'type': 'work', 'value': self.email or ''}]
                    if (email_addresses_old):
                        for email_address in email_addresses_old:
                            email_type = email_address.get('type')
                            if ((not email_type) or (email_type != 'work')):
                                email_addresses_new.append(email_address)

                    urls_new = [{'type': 'work', 'value': self.website or ''}]
                    if (urls_old):
                        for url in urls_old:
                            url_type = url.get('type')
                            if ((not url_type) or (url_type != 'work')):
                                urls_new.append(url)

                    service.people().updateContact(
                        resourceName=self.google_resource_name,
                        updatePersonFields='organizations,addresses,phoneNumbers,emailAddresses,urls,biographies,birthdays',
                        body={
                            'etag': etag,
                            'organizations': [{'name': organizationName}],
                            'addresses': addresses_new,
                            'phoneNumbers': phone_numbers_new,
                            'emailAddresses': email_addresses_new,
                            'urls': urls_new,
                            'biographies': [{'value': self.comment or ''}],
                            'birthdays': birthdays,
                        }
                    ).execute()
                    for child_id in self.child_ids:
                        if (child_id.google_resource_name):
                            person = service.people().get(
                                resourceName=child_id.google_resource_name,
                                personFields='addresses',
                            ).execute()
                            etag = person.get('etag')
                            addresses_old = person.get('addresses', [])

                            addresses_new = [{
                                'type': 'work',
                                'streetAddress': self.street or '',
                                'extendedAddress': extendedAddress,
                                'poBox': poBox,
                                'postalCode': self.zip or '',
                                'city': self.city or '',
                                'country': self.country_id.name or '',
                                'countryCode': self.country_id.code or '',
                            }]
                            if (addresses_old):
                                for address in addresses_old:
                                    address_type = address.get('type')
                                    if ((not address_type) or (address_type != 'work')):
                                        addresses_new.append(address)

                            service.people().updateContact(
                                resourceName=child_id.google_resource_name,
                                updatePersonFields='organizations,addresses',
                                body={
                                    'etag': etag,
                                    'organizations': [{'name': organizationName}],
                                    'addresses': addresses_new,
                                }
                            ).execute()
                else:
                    familyName = None
                    if (self.lastname):
                        familyName = self.lastname
                    givenName = None
                    if (self.firstname):
                        givenName = self.firstname
                    if (self.parent_id):
                        if (    (not familyName)
                            and (not givenName)
                            and (self.type in ['invoice', 'delivery', 'other'])
                        ):
                            familyName = dict(self.fields_get(['type'])['type']['selection'])[self.type]
                        if ((familyName) or (givenName)):
                            phone_numbers_new = [
                                {'type': 'work', 'value': self.phone or ''},
                                {'type': 'mobile', 'value': self.mobile or ''}
                            ]
                            if (phone_numbers_old):
                                for phone_number in phone_numbers_old:
                                    phone_type = phone_number.get('type')
                                    if ((not phone_type) or (not phone_type in ['work', 'mobile'])):
                                        phone_numbers_new.append(phone_number)

                            email_addresses_new = [{'type': 'work', 'value': self.email or ''}]
                            if (email_addresses_old):
                                for email_address in email_addresses_old:
                                    email_type = email_address.get('type')
                                    if ((not email_type) or (email_type !='work')):
                                        email_addresses_new.append(email_address)

                            urls_new = [{'type': 'work', 'value': self.website or ''}]
                            if (urls_old):
                                for url in urls_old:
                                    url_type = url.get('type')
                                    if ((not url_type) or (url_type !='work')):
                                        urls_new.append(url)

                            service.people().updateContact(
                                resourceName=self.google_resource_name,
                                updatePersonFields='names,phoneNumbers,emailAddresses,urls,biographies,birthdays',
                                body={
                                    'etag': etag,
                                    'names': [{'familyName': familyName or '', 'givenName': givenName or ''}],
                                    'phoneNumbers': phone_numbers_new,
                                    'emailAddresses': email_addresses_new,
                                    'urls': urls_new,
                                    'biographies': [{'value': self.comment or ''}],
                                    'birthdays': birthdays,
                                }
                            ).execute()
                    else:
                        if ((familyName) or (givenName)):
                            extendedAddress, poBox = self._get_extendedAddress_and_poBox()
                            addresses_new = [{
                                'type': 'home',
                                'streetAddress': self.street or '',
                                'extendedAddress': extendedAddress,
                                'poBox': poBox,
                                'postalCode': self.zip or '',
                                'city': self.city or '',
                                'country': self.country_id.name or '',
                                'countryCode': self.country_id.code or '',
                            }]
                            if (addresses_old):
                                for address in addresses_old:
                                    address_type = address.get('type')
                                    if ((not address_type) or (address_type !='home')):
                                        addresses_new.append(address)

                            phone_numbers_new = [
                                {'type': 'home', 'value': self.phone or ''},
                                {'type': 'mobile', 'value': self.mobile or ''},
                            ]
                            if (phone_numbers_old):
                                for phone_number in phone_numbers_old:
                                    phone_type = phone_number.get('type')
                                    if ((not phone_type) or (not phone_type in ['home', 'mobile'])):
                                        phone_numbers_new.append(phone_number)

                            email_addresses_new = [{'type': 'home', 'value': self.email or ''}]
                            if (email_addresses_old):
                                for email_address in email_addresses_old:
                                    email_type = email_address.get('type')
                                    if ((not email_type) or (email_type !='home')):
                                        email_addresses_new.append(email_address)

                            urls_new = [{'type': 'home', 'value': self.website or ''}]
                            if (urls_old):
                                for url in urls_old:
                                    url_type = url.get('type')
                                    if ((not url_type) or (url_type !='home')):
                                        urls_new.append(url)

                            service.people().updateContact(
                                resourceName=self.google_resource_name,
                                updatePersonFields='names,addresses,phoneNumbers,emailAddresses,urls,biographies,birthdays',
                                body={
                                    'etag': etag,
                                    'names': [{'familyName': familyName or '', 'givenName': givenName or ''}],
                                    'addresses': addresses_new,
                                    'phoneNumbers': phone_numbers_new,
                                    'emailAddresses': email_addresses_new,
                                    'urls': urls_new,
                                    'biographies': [{'value': self.comment or ''}],
                                    'birthdays': birthdays,
                                }
                            ).execute()
        except:
            pass

        return result

    @api.multi
    def upload_contact(self):
        google_people_sync_settings_id = self.env['google_people_sync.settings'].search([], limit=1)
        if (google_people_sync_settings_id):
            view_id = self.env['google_people_sync.upload_contact']
            new = view_id.create({'google_people_sync_settings_id': google_people_sync_settings_id.id})
            ctx = {'partner_ids': self._context.get('active_ids')}
            return {
                'type': 'ir.actions.act_window',
                'name': _('Confirmation'),
                'res_model': 'google_people_sync.upload_contact',
                'view_type': 'form',
                'view_mode': 'form',
                'res_id': new.id,
                'view_id': self.env.ref('google_people_sync.upload_contact_form', False).id,
                'target': 'new',
                'context': ctx,
            }

    @api.model
    def upload_contact_ok(self, google_people_sync_settings_id):
        if (not self.google_people_sync_settings_id):
            token = google_people_sync_settings_id.token
            service = build('people', 'v1', http=file.Storage(token).get().authorize(Http()))

            result = None
            if (self.is_company):
                if (self.name):
                    result = service.people().createContact(
                        body={
                            'organizations': [{'name': self.name}],
                        }
                    ).execute()
            else:
                familyName = None
                if (self.lastname):
                    familyName = self.lastname
                givenName = None
                if (self.firstname):
                    givenName = self.firstname
                if (    (not familyName)
                    and (not givenName)
                    and (self.parent_id)
                    and (self.type in ['invoice', 'delivery', 'other'])
                ):
                    familyName = dict(self.fields_get(['type'])['type']['selection'])[self.type]
                if ((familyName) or (givenName)):
                    result = service.people().createContact(
                        body={
                            'names': [{'familyName': familyName or '', 'givenName': givenName or ''}],
                        }
                    ).execute()

            if (result):
                resourceName = result.get('resourceName')
                if (google_people_sync_settings_id.odoo_group):
                    service.contactGroups().members().modify(
                        resourceName=google_people_sync_settings_id.odoo_group,
                        body={'resourceNamesToAdd': [resourceName]},
                    ).execute()

                # TODO: Anstatt 5 Sekunden warten, den neuen Datensatz komplett speichern und in "write" überspringen
                time.sleep(6)  # 4 --> fail; 5 --> ok; 6 --> ok incl. reserve
                self.write({
                    'google_people_sync_settings_id': google_people_sync_settings_id.id,
                    'google_resource_name': resourceName,
                })

    @api.multi
    def delete_contact(self):
        view_id = self.env['google_people_sync.delete_contact']
        new = view_id.create({})
        ctx = {'partner_ids': self._context.get('active_ids')}
        return {
            'type': 'ir.actions.act_window',
            'name': _('Confirmation'),
            'res_model': 'google_people_sync.delete_contact',
            'view_type': 'form',
            'view_mode': 'form',
            'res_id': new.id,
            'view_id': self.env.ref('google_people_sync.delete_contact_form', False).id,
            'target': 'new',
            'context': ctx,
        }


class GooglePeopleSyncUploadContact(models.TransientModel):

    _name = 'google_people_sync.upload_contact'
    _description = 'Upload Contact'

    google_people_sync_settings_id = fields.Many2one('google_people_sync.settings', string='Settings', required=True)

    @api.multi
    def upload_contact_ok(self):
        partner_ids = self._context.get('partner_ids')
        for partner_id in self.env['res.partner'].search([('id', 'in', partner_ids)]):
            partner_id.upload_contact_ok(self.google_people_sync_settings_id)


class GooglePeopleSyncDeleteContact(models.TransientModel):

    _name = 'google_people_sync.delete_contact'
    _description = 'Delete Google Contact'

    delete_type = fields.Selection(delete_type, string='Google Contact', default='remove', required=True)

    @api.multi
    def delete_contact_ok(self):
        partner_ids = self._context.get('partner_ids')
        for partner_id in self.env['res.partner'].search([('id', 'in', partner_ids)]):
            if (    (partner_id.google_resource_name)
                and (partner_id.google_people_sync_settings_id)
                and ((self.delete_type == 'delete') or (self.delete_type == 'remove'))
            ):
                token = partner_id.google_people_sync_settings_id.token
                service = build('people', 'v1', http=file.Storage(token).get().authorize(Http()))
                if (self.delete_type == 'delete'):
                    service.people().deleteContact(
                        resourceName=partner_id.google_resource_name,
                    ).execute()
                if (self.delete_type == 'remove'):
                    if (partner_id.google_people_sync_settings_id.odoo_group):
                        service.contactGroups().members().modify(
                            resourceName=partner_id.google_people_sync_settings_id.odoo_group,
                            body={'resourceNamesToRemove': [partner_id.google_resource_name]},
                        ).execute()
                    else:
                        service.contactGroups().members().modify(
                            resourceName=partner_id.google_people_sync_settings_id.private_group,
                            body={'resourceNamesToAdd': [partner_id.google_resource_name]},
                        ).execute()

            partner_id.unlink()