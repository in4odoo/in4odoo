# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.tools import config
from datetime import datetime
from dateutil import parser
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

# from googleapiclient.discovery import build
# https://pypi.org/project/google-api-python-client
#from . import googleapiclient
#from .googleapiclient import discovery
from .googleapiclient.discovery import build
from .googleapiclient.errors import HttpError

# from httplib2 import Http
# https://github.com/httplib2/httplib2
#from . import httplib2
from .httplib2 import Http

# from oauth2client import file, client, tools
# https://github.com/googleapis/oauth2client
from . import oauth2client
from .oauth2client import file, client, tools

import base64
import pytz
import time
import urllib

import logging
_logger = logging.getLogger(__name__)
#_logger.info()
#_logger.warning()


# Link to enable the Gmail API: https://developers.google.com/gmail/api/quickstart/python
#SCOPES = 'https://www.googleapis.com/auth/gmail.readonly'
#SCOPES = 'https://www.googleapis.com/auth/gmail'

# Link to enable the People API: https://developers.google.com/people/quickstart/python
#SCOPES = 'https://www.googleapis.com/auth/contacts.readonly'
#SCOPES = 'https://www.googleapis.com/auth/contacts'

# Link to download the latest client library for Python: https://developers.google.com/api-client-library/python/start/installation


# If modifying these scopes, delete the file token.json.
SCOPES = 'https://www.googleapis.com/auth/contacts'


download_selection = [
    ('individuals', 'Individuals'),
    ('companies', 'Companies'),
    ('individuals_companies', 'Individuals and Companies'),
    ('individuals_companies_contacts', 'Individuals, Companies and Contacts'),
]

upload_selection = [
    ('manually_write', 'Save changes and upload new contacts manually'),
    ('auto_upload', 'Save changes and upload new contacts automatic'),
    ('auto_write', 'Save changes automatic and upload new contacts manually'),
]


class GooglePeopleSyncSettings(models.Model):

    _name = 'google_people_sync.settings'
    _description = 'Settings'
    _order = 'name'

    name = fields.Char(string='Name', required=True)
    active = fields.Boolean(string='Active', default=True)
    birthday_field = fields.Char(string='Birthday Field')
    po_box_list = fields.Text(string='P.O. Box - Names', default='Postfach\nCase Postale\nCasella Postale\nPost Office Box\nPost-Office Box\nP.O. Box\nP.O.B\nPOB')
    download_selection = fields.Selection(download_selection, string='Download', default='individuals_companies_contacts', required=True)
    upload_selection = fields.Selection(upload_selection, string='Upload', default='auto_write', required=True)

    token = fields.Char(string='Save Token to', compute='compute_token', store=True)

    odoo_group = fields.Char(string='Odoo Group')
    private_group = fields.Char(string='Private Group', required=True, default='contactGroups/???')
    group_list = fields.Text(string='Existing Groups', readonly=True)

    download_timestamp = fields.Datetime(string='Last Download')

    @api.depends('name')
    def compute_token(self):
        filestore = config.filestore(self._cr.dbname)
        for google_people_sync_settings_id in self:
            token = filestore + '/google_people_sync/' + str(google_people_sync_settings_id.id) + '/token.json'
            google_people_sync_settings_id.token = token

    @api.model
    def load_group_list(self):
        google_people_sync_settings_id = None

        context = dict(self._context or {})
        active_id = context.get('active_id', False)
        if (active_id):
            google_people_sync_settings_id = self.env['google_people_sync.settings'].browse(active_id)

        if (google_people_sync_settings_id):
            token = google_people_sync_settings_id.token
            service = build('people', 'v1', http=file.Storage(token).get().authorize(Http()))

            group_list = []
            next_page_token = ''
            while True:
                result = service.contactGroups().list(
                    pageToken=next_page_token,
                ).execute()
                contact_groups = result.get('contactGroups', [])

                if not contact_groups:
                    break

                for contact_group in contact_groups:
                    if (contact_group.get('groupType') == 'USER_CONTACT_GROUP'):
                        group_list.append(contact_group.get('name') + ': ' + contact_group.get('resourceName'))

                next_page_token = result.get('nextPageToken', [])
                if not next_page_token:
                    break

            group_list.sort()
            group_list_text = ''
            for group in group_list:
                group_list_text += group + '\n'
            google_people_sync_settings_id.sudo().write({
                'group_list': group_list_text,
            })

    @api.model
    def _set_address(self, address, values):
        has_values = False
        address_type = address.get('type')
        if (   (not address_type)
            or ((values['is_company']) and (address_type == 'work'))
            or ((not values['is_company']) and (address_type == 'home'))
        ):
            value = address.get('streetAddress')
            if (value):
                values['street'] = value
                has_values = True
            value = address.get('extendedAddress')
            if (not value):
                value = address.get('poBox')
                has_values = True
            if (value):
                values['street2'] = value
                has_values = True
            value = address.get('postalCode')
            if (value):
                values['zip'] = value
                has_values = True
            value = address.get('city')
            if (value):
                values['city'] = value
                has_values = True

            country_id = None
            if (not country_id):
                country_code = address.get('countryCode')
                if (country_code):
                    domain = [('code', '=', country_code)]
                    country_id = self.env['res.country'].search(domain, limit=1)
            if (not country_id):
                country_name = address.get('country')
                if (country_name):
                    domain = [('name', '=', country_name)]
                    country_id = self.env['res.country'].search(domain, limit=1)
            if (not country_id):
                country_code = 'CH'
                if (country_code):
                    domain = [('code', '=', country_code)]
                    country_id = self.env['res.country'].search(domain, limit=1)
            if (country_id):
                values['country_id'] = country_id.id

        return has_values

    @api.model
    def _set_photo(self, photo, values):
        has_values = False
        if (photo.get('metadata').get('source').get('type') == 'CONTACT'):
            default = photo.get('default')
            if (not default):
                url = photo.get('url')
                if (url):
                    link = urllib.request.urlopen(url).read()
                    value = base64.encodebytes(link)
                    values['image'] = value

        return has_values

    @api.model
    def _set_phone_number(self, phone_number, values, is_company_or_contact):
        has_values = False
        value = phone_number.get('value')
        if (value):
            phone_type = phone_number.get('type')
            if (   (not phone_type)
                or ((is_company_or_contact) and (phone_type == 'work'))
                or ((not is_company_or_contact) and (phone_type == 'home'))
            ):
                values['phone'] = value
                has_values = True
            if (phone_type == 'mobile'):
                values['mobile'] = value

        return has_values

    @api.model
    def _set_email_address(self, email_address, values, is_company_or_contact):
        has_values = False
        value = email_address.get('value')
        if (value):
            email_type = email_address.get('type')
            if (   (not email_type)
                or ((is_company_or_contact) and (email_type == 'work'))
                or ((not is_company_or_contact) and (email_type == 'home'))
            ):
                values['email'] = value
                has_values = True

        return has_values

    @api.model
    def _set_url(self, url, values, is_company_or_contact):
        has_values = False
        if (url.get('metadata').get('source').get('type') == 'CONTACT'):
            value = url.get('value')
            if (value):
                if ((not value.startswith('http://')) and (not value.startswith('https://'))):
                    value = 'http://' + value
                url_type = url.get('type')
                if (   ((is_company_or_contact) and (url_type == 'work'))
                    or ((not is_company_or_contact) and (url_type == 'homePage'))
                ):
                    values['website'] = value
                    has_values = True

        return has_values

    @api.model
    def _set_biographie(self, biographie, values):
        has_values = False
        value = biographie.get('value')
        if (value):
            values['comment'] = value

        return has_values

    @api.model
    def _set_membership(self, membership, values):
        contactGroupId = membership.get('contactGroupMembership').get('contactGroupId')
        if (contactGroupId):
            domain = [('google_group', '=', 'contactGroups/' + contactGroupId)]
            category_id = self.env['res.partner.category'].search(domain, limit=1)
            if (category_id):
                values['category_id'].append((4, category_id.id))

    @api.model
    def _set_birthday(self, birthday, values, birthday_field):
        has_values = False
        if (birthday.get('metadata').get('source').get('type') == 'CONTACT'):
            value = birthday.get('date')
            if (value):
                try:
                    values[birthday_field] = datetime.strptime(str(value.get('year')) + '-' + str(value.get('month')) + '-' + str(value.get('day')), '%Y-%m-%d')
                except:
                    pass

        return has_values

    @api.model
    def _update_contact(self, google_people_sync_settings_id, connection, values, with_empty_contacts):
        log_create = None
        log_ignore = None
        log_write = None

        has_values = False
        if (not values['parent_id']):
            addresses = connection.get('addresses', [])
            for address in addresses:
                if (self._set_address(address, values)):
                    has_values = True

        is_company_or_contact = (values['is_company']) or (values['parent_id'])

        photos = connection.get('photos', [])
        for photo in photos:
            if (self._set_photo(photo, values)):
                has_values = True

        phone_numbers = connection.get('phoneNumbers', [])
        for phone_number in phone_numbers:
            if (self._set_phone_number(phone_number, values, is_company_or_contact)):
                has_values = True

        email_addresses = connection.get('emailAddresses', [])
        for email_address in email_addresses:
            if (self._set_email_address(email_address, values, is_company_or_contact)):
                has_values = True

        urls = connection.get('urls', [])
        for url in urls:
            if (self._set_url(url, values, is_company_or_contact)):
                has_values = True

        biographies = connection.get('biographies', [])
        for biographie in biographies:
            if (self._set_biographie(biographie, values)):
                has_values = True

        memberships = connection.get('memberships', [])
        for membership in memberships:
            if (self._set_membership(membership, values)):
                has_values = True

        if (google_people_sync_settings_id.birthday_field):
            birthdays = connection.get('birthdays', [])
            for birthday in birthdays:
                if (self._set_birthday(birthday, values, google_people_sync_settings_id.birthday_field)):
                    has_values = True

        values['google_people_sync_settings_id'] = google_people_sync_settings_id.id
        values['google_resource_name'] = connection.get('resourceName')
        values['google_download_timestamp'] = fields.datetime.now()

        domain = [
            ('parent_id', '=', values['parent_id'] or False),
            ('google_people_sync_settings_id', '=', values['google_people_sync_settings_id']),
            ('google_resource_name', '=', values['google_resource_name']),
        ]
        local_partner_id = self.env['res.partner'].with_context(active_test=False).search(domain, limit=1)
        if (local_partner_id):
            for category_id in local_partner_id.category_id:
                if (category_id.google_group):
                    found = False
                    for new_category_id in values['category_id']:
                        if (new_category_id[1] == category_id.id):
                            found = True
                    if (not found):
                        values['category_id'].append((3, category_id.id))
            local_partner_id.write(values)
            log_write = local_partner_id.display_name
        else:
            if ((has_values) or (with_empty_contacts)):
                local_partner_id = self.env['res.partner'].with_context(install_mode=True).create(values) #install_mode=True --> no default image
                log_create = local_partner_id.display_name
            else:
                if ((values['lastname']) and (values['firstname'])):
                    log_ignore = values['lastname'] + ' ' + values['firstname']
                elif (values['lastname']):
                    log_ignore = values['lastname']
                elif (values['firstname']):
                    log_ignore = values['firstname']
                else:
                    log_ignore = '? (unknown)'

        return log_create, log_ignore, log_write

    @api.model
    def _update_individual(self, google_people_sync_settings_id, connection, person, organization_partner_id, has_organization):
        log_create = None
        log_ignore = None
        log_write = None

        values = {
            'is_company': False,
            'parent_id': None,
            'lastname': None,
            'firstname': None,
            #'image': None, --> no Upload!!!
            'street': None,
            'street2': None,
            'zip': None,
            'city': None,
            'country_id': None,
            'phone': None,
            'mobile': None,
            'email': None,
            'website': None,
            'comment': None,
            'category_id': [],
        }
        if (google_people_sync_settings_id.birthday_field):
            values[google_people_sync_settings_id.birthday_field] = None

        display_name = person.get('displayName')
        if (display_name):
            value = person.get('familyName')
            if (value):
                values['lastname'] = value
            value = person.get('givenName')
            if (value):
                values['firstname'] = value
            if (organization_partner_id):
                values['parent_id'] = organization_partner_id.id
                if ((values['lastname']) and (not values['firstname'])):
                    if (values['lastname'] == dict(organization_partner_id.fields_get(['type'])['type']['selection'])['invoice']):
                        values['type'] = 'invoice'
                        values['lastname'] = None
                    elif (values['lastname'] == dict(organization_partner_id.fields_get(['type'])['type']['selection'])['delivery']):
                        values['type'] = 'delivery'
                        values['lastname'] = None
                    elif (values['lastname'] == dict(organization_partner_id.fields_get(['type'])['type']['selection'])['other']):
                        values['type'] = 'other'
                        values['lastname'] = None
            with_empty_contacts = True
            if (    (has_organization)
                and (not organization_partner_id)
                and (google_people_sync_settings_id.download_selection == 'individuals_companies_contacts')
            ):
                with_empty_contacts = False
            log_create, log_ignore, log_write = self._update_contact(google_people_sync_settings_id, connection, values, with_empty_contacts)
            #if (display_name == 'Simon Gnehm'):
            #    _logger.info(display_name)

        return log_create, log_ignore, log_write

    @api.model
    def _update_company(self, google_people_sync_settings_id, connection, organization):
        log_create = None
        log_ignore = None
        log_write = None

        values = {
            'is_company': True,
            'parent_id': None,
            'lastname': None,
            'firstname': None,
            #'image': None, --> no Upload!!!
            'street': None,
            'street2': None,
            'zip': None,
            'city': None,
            'country_id': None,
            'phone': None,
            'mobile': None,
            'email': None,
            'website': None,
            'comment': None,
            'category_id': [],
        }
        if (google_people_sync_settings_id.birthday_field):
            values[google_people_sync_settings_id.birthday_field] = None

        display_name = organization.get('name')
        if (display_name):
            values['lastname'] = display_name
            log_create, log_ignore, log_write = self._update_contact(google_people_sync_settings_id, connection, values, True)
            #if (display_name == 'Gnehm Informatik GmbH'):
            #    _logger.info(display_name)

        return log_create, log_ignore, log_write

    @api.model
    def _download_connections(self, service, google_people_sync_settings_id, only_contacts):
        added_list = []
        ignored_list = []
        changed_list = []
        missing_list = []

        next_page_token = ''
        while True:
            result = service.people().connections().list(
                resourceName='people/me',
                pageToken=next_page_token,
                personFields='addresses,biographies,birthdays,emailAddresses,memberships,metadata,names,organizations,phoneNumbers,photos,urls',
                # personFields='addresses,ageRanges,biographies,birthdays,braggingRights,coverPhotos,emailAddresses,events,genders,imClients,interests,locales,memberships,metadata,names,nicknames,occupations,organizations,phoneNumbers,photos,relations,relationshipInterests,relationshipStatuses,residences,sipAddresses,skills,taglines,urls,userDefined',
            ).execute()
            connections = result.get('connections', [])

            if not connections:
                break

            for connection in connections:
                need_update = True

                if (need_update):
                    odoo_group = None
                    if (google_people_sync_settings_id.odoo_group):
                        odoo_group = google_people_sync_settings_id.odoo_group.split('/')[1]
                    if (odoo_group):
                        in_odoo_group = False
                    else:
                        in_odoo_group = True
                    private_group = google_people_sync_settings_id.private_group.split('/')[1]
                    memberships = connection.get('memberships', [])
                    for membership in memberships:
                        contactGroupId = membership.get('contactGroupMembership').get('contactGroupId')
                        if (contactGroupId):
                            if ((need_update) and (private_group) and (contactGroupId == private_group)):
                                need_update = False
                            if ((not in_odoo_group) and (odoo_group) and (contactGroupId == odoo_group)):
                                in_odoo_group = True
                    if (not in_odoo_group):
                        need_update = False

                if (need_update):
                    updateTime = connection.get('metadata').get('sources')[0].get('updateTime')
                    if ((updateTime) and (google_people_sync_settings_id.download_timestamp)):
                        last_download_str = google_people_sync_settings_id.download_timestamp
                        last_download_datetime = datetime.strptime(last_download_str, DEFAULT_SERVER_DATETIME_FORMAT)
                        if (parser.parse(updateTime) < pytz.UTC.localize(last_download_datetime)):
                            need_update = False

                if (need_update):
                    people_count = 0

                    if (    (people_count == 0)
                        and (not only_contacts)
                        and (google_people_sync_settings_id.download_selection in [
                                'individuals',
                                'individuals_companies',
                                'individuals_companies_contacts',
                            ])
                    ):
                        has_organization = False
                        organizations = connection.get('organizations', [])
                        for organization in organizations:
                            if (organization.get('metadata').get('source').get('type') == 'CONTACT'):
                                has_organization = True
                                break

                        persons = connection.get('names', [])
                        for person in persons:
                            if (person.get('metadata').get('source').get('type') == 'CONTACT'):
                                log_create, log_ignore, log_write = self._update_individual(
                                    google_people_sync_settings_id,
                                    connection,
                                    person,
                                    None,
                                    has_organization,
                                )
                                if (log_create):
                                    people_count += 1
                                    added_list.append(log_create)
                                if (log_ignore):
                                    people_count += 1
                                    ignored_list.append(log_ignore)
                                if (log_write):
                                    people_count += 1
                                    changed_list.append(log_write)
                            if (people_count > 0):
                                break


                    if (    (people_count == 0)
                        and (google_people_sync_settings_id.download_selection in [
                                'companies',
                                'individuals_companies',
                                'individuals_companies_contacts',
                            ])
                    ):
                        organizations = connection.get('organizations', [])
                        for organization in organizations:
                            if (organization.get('metadata').get('source').get('type') == 'CONTACT'):
                                if (only_contacts):
                                    persons = connection.get('names', [])
                                    organization_name = organization.get('name')
                                    domain = [
                                        ('is_company', '=', True),
                                        ('parent_id', '=', False),
                                        ('name', '=', organization_name),
                                    ]
                                    organization_partner_id = self.env['res.partner'].with_context(active_test=False).search(domain, limit=1)
                                    if (organization_partner_id):
                                        for person in persons:
                                            if (person.get('metadata').get('source').get('type') == 'CONTACT'):
                                                log_create, log_ignore, log_write = self._update_individual(
                                                    google_people_sync_settings_id,
                                                    connection,
                                                    person,
                                                    organization_partner_id,
                                                    True,
                                                )
                                                if (log_create):
                                                    people_count += 1
                                                    added_list.append(log_create)
                                                if (log_ignore):
                                                    people_count += 1
                                                    ignored_list.append(log_ignore)
                                                if (log_write):
                                                    people_count += 1
                                                    changed_list.append(log_write)
                                            if (people_count > 0):
                                                break
                                    else:
                                        for person in persons:
                                            if (person.get('metadata').get('source').get('type') == 'CONTACT'):
                                                msg = organization_name + ' (' + person.get('displayName') + ')'
                                                missing_list.append(msg)
                                else:
                                    log_create, log_ignore, log_write = self._update_company(
                                        google_people_sync_settings_id,
                                        connection,
                                        organization,
                                    )
                                    if (log_create):
                                        people_count += 1
                                        added_list.append(log_create)
                                    if (log_ignore):
                                        people_count += 1
                                        ignored_list.append(log_ignore)
                                    if (log_write):
                                        people_count += 1
                                        changed_list.append(log_write)

                            if (people_count > 0):
                                break

            next_page_token = result.get('nextPageToken', [])
            if not next_page_token:
                break

        return added_list, ignored_list, changed_list, missing_list

    @api.model
    def download_contacts(self, id=None):
        google_people_sync_settings_id = None

        if (id):
            google_people_sync_settings_id = id
        else:
            context = dict(self._context or {})
            active_id = context.get('active_id', False)
            if (active_id):
                google_people_sync_settings_id = self.env['google_people_sync.settings'].browse(active_id)

        if (google_people_sync_settings_id):
            start_date = time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)

            token = google_people_sync_settings_id.token
            service = build('people', 'v1', http=file.Storage(token).get().authorize(Http()))

            added_list = []
            ignored_list = []
            changed_list = []
            missing_list = []

            # Private Adressen und Unternehmen
            log_create, log_ignore, log_write, log_missing = self._download_connections(service, google_people_sync_settings_id, False)
            added_list.extend(log_create)
            ignored_list.extend(log_ignore)
            changed_list.extend(log_write)
            missing_list.extend(log_missing)

            # Geschäftliche Adressen
            if (google_people_sync_settings_id.download_selection == 'individuals_companies_contacts'):
                log_create, log_ignore, log_write, log_missing = self._download_connections(service, google_people_sync_settings_id, True)
                added_list.extend(log_create)
                ignored_list.extend(log_ignore)
                changed_list.extend(log_write)
                missing_list.extend(log_missing)

            added_list.sort()
            added_list_text = ''
            for log_create_text in added_list:
                added_list_text += log_create_text + '\n'

            ignored_list.sort()
            ignored_list_text = ''
            for log_ignore_text in ignored_list:
                ignored_list_text += log_ignore_text + '\n'

            changed_list.sort()
            changed_list_text = ''
            for log_write_text in changed_list:
                changed_list_text += log_write_text + '\n'

            missing_list.sort()
            missing_list_text = ''
            for log_missing_text in missing_list:
                missing_list_text += log_missing_text + '\n'

            google_people_sync_settings_id.sudo().write({
                'download_timestamp': fields.datetime.now(),
            })

            end_date = time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)

            summary = '''
Download started: %s
Download finished: %s

Added records: %d
Ignored records: %d
Changed records: %d
Missing companies: %d
            ''' % (
                start_date,
                end_date,
                len(added_list),
                len(ignored_list),
                len(changed_list),
                len(missing_list),
            )
            summary = summary.strip() + '\n\n'
            summary += 'Added records:\n' + added_list_text
            summary += '\n'
            summary += 'Ignored records:\n' + ignored_list_text
            summary += '\n'
            summary += 'Changed records:\n' + changed_list_text
            summary += '\n'
            summary += 'Missing companies:\n' + missing_list_text
            self.env['google_people_sync.request'].sudo().create({
                'name': 'Download report',
                'date': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                'body': summary,
            })

    @api.model
    def delete_contacts(self, id=None):
        google_people_sync_settings_id = None

        if (id):
            google_people_sync_settings_id = id
        else:
            context = dict(self._context or {})
            active_id = context.get('active_id', False)
            if (active_id):
                google_people_sync_settings_id = self.env['google_people_sync.settings'].browse(active_id)

        if (google_people_sync_settings_id):
            start_date = time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)

            token = google_people_sync_settings_id.token
            service = build('people', 'v1', http=file.Storage(token).get().authorize(Http()))

            deleted_list = []
            archived_list = []

            domain = [
                ('google_resource_name', '!=', False),
                ('google_people_sync_settings_id', '=', google_people_sync_settings_id.id),
            ]
            partner_ids = self.env['res.partner'].search(domain)
            for partner_id in partner_ids:
                try:
                    person = service.people().get(
                        resourceName = partner_id.google_resource_name,
                        personFields='metadata',
                    ).execute()
                except HttpError as e:
                    if (e.resp.status == 404):
                        current_partner_name = partner_id.name
                        self.env.cr.autocommit(False)
                        try:
                            partner_id.unlink()
                            deleted_list.append(current_partner_name)
                        except:
                            self.env.cr.rollback()
                            values = {
                                'active': False,
                                'google_people_sync_settings_id': None,
                                'google_resource_name': None,
                                'google_download_timestamp': None,
                            }
                            partner_id.write(values)
                            archived_list.append(current_partner_name)
                            pass
                    else:
                        pass

            deleted_list.sort()
            deleted_list_text = ''
            for log_deleted_text in deleted_list:
                deleted_list_text += log_deleted_text + '\n'

            archived_list.sort()
            archived_list_text = ''
            for log_archived_text in archived_list:
                archived_list_text += log_archived_text + '\n'

            end_date = time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)

            summary = '''
Deletion started: %s
Deletion finished: %s

Deleted records: %d
Archived records: %d
            ''' % (
                start_date,
                end_date,
                len(deleted_list),
                len(archived_list),
            )
            summary = summary.strip() + '\n\n'
            summary += 'Deleted records:\n' + deleted_list_text
            summary += '\n'
            summary += 'Archived records:\n' + archived_list_text
            self.env['google_people_sync.request'].sudo().create({
                'name': 'Deletion report',
                'date': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                'body': summary,
            })

    @api.model
    def download_contacts_all_settings(self):
        google_people_sync_settings_ids = self.env['google_people_sync.settings'].search([('active', '=', True)])
        for google_people_sync_settings_id in google_people_sync_settings_ids:
            self.download_contacts(google_people_sync_settings_id)

    @api.model
    def delete_contacts_all_settings(self):
        google_people_sync_settings_ids = self.env['google_people_sync.settings'].search([('active', '=', True)])
        for google_people_sync_settings_id in google_people_sync_settings_ids:
            self.delete_contacts(google_people_sync_settings_id)


class GooglePeopleSyncRequest(models.Model):

    _name = 'google_people_sync.request'
    _description = 'Request'
    _order = 'date desc'

    name = fields.Char('Name', required=True)
    date = fields.Datetime('Date')
    body = fields.Text('Request', readonly=True)