# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* amway_survey
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 11.0-20190127\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-01-27 08:13+0000\n"
"PO-Revision-Date: 2019-01-27 08:13+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: amway_survey
#: model:ir.ui.view,arch_db:amway_survey.crs_template
msgid "&amp;nbsp;"
msgstr "&amp;nbsp;"

#. module: amway_survey
#: model:ir.ui.view,arch_db:amway_survey.crs_template
msgid "<font style=\"font-size: 11px;\"><br/></font>\n"
"                                    <font style=\"font-size: 2px;\"><br/></font>\n"
"                                    <font style=\"font-size: 11px;\"><span>Bitte notieren Sie Ihren jeweiligen Messwert in der Skala von 50-100 % bzw. von 50-0 %. Zu jedem Messwert erhalten Sie eine kurze Erläuterung sowie</span><br/></font>\n"
"                                    <font style=\"font-size: 11px;\"><span>allgemeine Anregungen zur Verbesserung der Werte. Wir beraten Sie gerne, welche für Sie die jeweils am besten geeigneten Massnahmen sind.</span></font>"
msgstr "<font style=\"font-size: 11px;\"><br/></font>\n"
"                                    <font style=\"font-size: 2px;\"><br/></font>\n"
"                                    <font style=\"font-size: 11px;\"><span>Bitte notieren Sie Ihren jeweiligen Messwert in der Skala von 50-100 % bzw. von 50-0 %. Zu jedem Messwert erhalten Sie eine kurze Erläuterung sowie</span><br/></font>\n"
"                                    <font style=\"font-size: 11px;\"><span>allgemeine Anregungen zur Verbesserung der Werte. Wir beraten Sie gerne, welche für Sie die jeweils am besten geeigneten Massnahmen sind.</span></font>"

#. module: amway_survey
#: model:ir.ui.view,arch_db:amway_survey.crs_template
msgid "<font style=\"font-size: 11px;\"><span> Ernährungstyp </span></font>"
msgstr "<font style=\"font-size: 11px;\"><span> Ernährungstyp </span></font>"

#. module: amway_survey
#: model:ir.ui.view,arch_db:amway_survey.crs_template
msgid "<font style=\"font-size: 11px;\"><strong><span>Aufklärender Hinweis</span></strong><br/></font>"
msgstr "<font style=\"font-size: 11px;\"><strong><span>Aufklärender Hinweis</span></strong><br/></font>"

#. module: amway_survey
#: model:ir.ui.view,arch_db:amway_survey.crs_template
msgid "<font style=\"font-size: 11px;\"><strong>Ihre CRS® Messergebnisse: Erstellen Sie Ihren individuellen Plan</strong><br/></font>\n"
"                                    <font style=\"font-size: 2px;\"><br/></font>\n"
"                                    <font style=\"font-size: 11px;\"><span>Persönlicher Gesundheitscheck von </span></font>"
msgstr "<font style=\"font-size: 11px;\"><strong>Ihre CRS® Messergebnisse: Erstellen Sie Ihren individuellen Plan</strong><br/></font>\n"
"                                    <font style=\"font-size: 2px;\"><br/></font>\n"
"                                    <font style=\"font-size: 11px;\"><span>Persönlicher Gesundheitscheck von </span></font>"

#. module: amway_survey
#: model:ir.ui.view,arch_db:amway_survey.crs_template
msgid "<font style=\"font-size: 9px;\">&amp;nbsp;</font>"
msgstr "<font style=\"font-size: 9px;\">&amp;nbsp;</font>"

#. module: amway_survey
#: model:ir.ui.view,arch_db:amway_survey.crs_template
msgid "<font style=\"font-size: 9px;\"><span>Datum der Messung/</span><br/><span>Folgemessungen</span></font>"
msgstr "<font style=\"font-size: 9px;\"><span>Datum der Messung/</span><br/><span>Folgemessungen</span></font>"

#. module: amway_survey
#: model:ir.ui.view,arch_db:amway_survey.crs_template
msgid "<font style=\"font-size: 9px;\">Bemerkungen</font>"
msgstr "<font style=\"font-size: 9px;\">Bemerkungen</font>"

#. module: amway_survey
#: model:ir.ui.view,arch_db:amway_survey.crs_template
msgid "<font style=\"font-size: 9px;\">Ihre Messwerte</font>"
msgstr "<font style=\"font-size: 9px;\">Ihre Messwerte</font>"

#. module: amway_survey
#: model:ir.ui.view,arch_db:amway_survey.res_partner_form
msgid "<span class=\"o_stat_text\">Answers</span>"
msgstr "<span class=\"o_stat_text\">Antworten</span>"

#. module: amway_survey
#: model:ir.actions.act_window,name:amway_survey.survey_user_input_res_partner_action
msgid "Answers"
msgstr "Antworten"

#. module: amway_survey
#: model:ir.ui.view,arch_db:amway_survey.crs_template
msgid "Bei der hier beschriebenen Methode handelt es sich um ein Verfahren, welches nicht zu den allgemein anerkann- ten Verfahren im Sinne einer Anerkennung durch die Schulmedizin gehört. Alle getroffenen Aussagen über Eigenschaften und Wirkungen sowie Indikatoren des vorgestellten Verfahrens beruhen auf den Erkenntnissen und Erfahrungswerten in der Therapierichtung selbst, die von der herschenden Schulmedizin nicht geteilt werden."
msgstr "Bei der hier beschriebenen Methode handelt es sich um ein Verfahren, welches nicht zu den allgemein anerkann- ten Verfahren im Sinne einer Anerkennung durch die Schulmedizin gehört. Alle getroffenen Aussagen über Eigenschaften und Wirkungen sowie Indikatoren des vorgestellten Verfahrens beruhen auf den Erkenntnissen und Erfahrungswerten in der Therapierichtung selbst, die von der herschenden Schulmedizin nicht geteilt werden."

#. module: amway_survey
#: model:ir.actions.report,name:amway_survey.crs_report
msgid "CRS"
msgstr "CRS"

#. module: amway_survey
#: model:ir.model,name:amway_survey.model_res_partner
msgid "Contact"
msgstr "Kontakt"

#. module: amway_survey
#: model:ir.model.fields,field_description:amway_survey.field_report_amway_survey_crs_template_display_name
msgid "Display Name"
msgstr "Anzeigename"

#. module: amway_survey
#: model:ir.model.fields,field_description:amway_survey.field_report_amway_survey_crs_template_id
msgid "ID"
msgstr "ID"

#. module: amway_survey
#: model:ir.model.fields,field_description:amway_survey.field_report_amway_survey_crs_template___last_update
msgid "Last Modified on"
msgstr "Zuletzt geändert am"

#. module: amway_survey
#: model:ir.ui.view,arch_db:amway_survey.survey_sfinished
msgid "Or you can"
msgstr "Oder Sie können"

#. module: amway_survey
#: model:ir.model.fields,field_description:amway_survey.field_res_partner_user_input_ids
#: model:ir.model.fields,field_description:amway_survey.field_res_users_user_input_ids
msgid "Responses"
msgstr "Antworten"

#. module: amway_survey
#: model:ir.ui.view,arch_db:amway_survey.survey_user_input_form
msgid "Show Answers"
msgstr "Antworten anzeigen"

#. module: amway_survey
#: model:ir.model,name:amway_survey.model_survey_user_input
msgid "Survey User Input"
msgstr "Benutzereingaben"

#. module: amway_survey
#: model:ir.model,name:amway_survey.model_survey_user_input_line
msgid "Survey User Input Line"
msgstr "Benuzereingabe"

#. module: amway_survey
#: model:ir.ui.view,arch_db:amway_survey.survey_sfinished
msgid "go back to the answer"
msgstr "zurück zur Antwort"

#. module: amway_survey
#: model:ir.model,name:amway_survey.model_report_amway_survey_crs_template
msgid "report.amway_survey.crs_template"
msgstr "report.amway_survey.crs_template"

