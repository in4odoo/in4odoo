# -*- coding: utf-8 -*-

from odoo import api, fields, models


class AmwaySurveySurveyUserInput(models.Model):

    _inherit = 'survey.user_input'
    _order = 'date_create desc'

    @api.multi
    def open_survey_action(self):
        self.ensure_one()
        if (self.state == 'done'):
            self.state = 'skip'
        return {
            'type': 'ir.actions.act_url',
            'name': 'Start Survey',
            'target': 'self',
            'url': self.with_context(relative_url=True).survey_id.public_url + '/' + self.token
        }

class AmwaySurveySurveyUserInputLine(models.Model):

    _inherit = 'survey.user_input_line'

    @api.model
    def save_line_numerical_box(self, user_input_id, question, post, answer_tag):
        vals = {
            'user_input_id': user_input_id,
            'question_id': question.id,
            'survey_id': question.survey_id.id,
            'skipped': False
        }
        old_uil = self.search([
            ('user_input_id', '=', user_input_id),
            ('survey_id', '=', question.survey_id.id),
            ('question_id', '=', question.id)
        ])
        old_uil.sudo().unlink()

        if answer_tag in post and post[answer_tag].strip():
            vals.update({'answer_type': 'number', 'value_number': float(post[answer_tag])})
        else:
            vals.update({'answer_type': None, 'skipped': True})

        # '-1' indicates 'comment count as an answer' so do not need to record it
        if post.get(answer_tag) and post.get(answer_tag) != '-1':
            self.create(vals)

        comment_answer = post.pop(("%s_%s" % (answer_tag, 'comment')), '').strip()
        if comment_answer:
            vals.update({
                'answer_type': 'text',
                'value_text': comment_answer,
                'value_number': None,
                'skipped': False,
                'value_suggested': False
            })
            self.create(vals)

        return True


class AmwaySurveyCRSTemplate(models.AbstractModel):

    _name = 'report.amway_survey.crs_template'

    @api.model
    def get_report_values(self, docids, data=None):
        domain = [
            ('survey_id.title', '=', 'CRS'),
            ('page_id.title', '=', 'Messergebnisse'),
            ('question', 'not like', 'Bemerkungen'),
        ]
        question_ids = self.env['survey.question'].search(domain)
        rows = []
        cols = []
        for question_id in question_ids:
            col = []
            col.append(question_id.question)
            col.append(question_id.description)

            values = []
            if (question_id.validation_required):
                if ((question_id.validation_min_float_value == 0) and (question_id.validation_max_float_value == 50)):
                    for i in range(11):
                        if (i == 10):
                            values.append(str(50 - i * 5) + '%')
                        else:
                            values.append(50 - i * 5)
                if ((question_id.validation_min_float_value == 50) and (question_id.validation_max_float_value == 100)):
                    for i in range(11):
                        if (i == 10):
                            values.append(str(50 + i * 5) + '%')
                        else:
                            values.append(50 + i * 5)
            elif (question_id.type == 'simple_choice'):
                for labels_id in question_id.labels_ids:
                    values.append(labels_id.value)
            col.append(values)

            if (len(cols) == 4):
                rows.append(cols)
                cols = []
            cols.append(col)
        if (len(cols) > 0):
            rows.append(cols)

        remarks = ''

        return {
            'doc_ids': docids,
            'doc_model': 'res.partner',
            'docs': self.env['res.partner'].browse(docids),
            'rows': rows,
        }