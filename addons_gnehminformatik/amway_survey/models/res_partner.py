# -*- coding: utf-8 -*-

from odoo import models, fields, api
from ast import literal_eval
from datetime import datetime
from dateutil.relativedelta import relativedelta


class AmwaySurveyResPartner(models.Model):

    _inherit = 'res.partner'

    user_input_ids = fields.One2many('survey.user_input', 'partner_id', string='Responses', readonly=True)

    @api.multi
    def open_answers(self):
        self.ensure_one()

        action = self.env.ref('amway_survey.survey_user_input_res_partner_action').read()[0]
        action['domain'] = literal_eval(action['domain'])
        action['domain'].append(('partner_id', 'child_of', self.id))

        return action