# -*- coding: utf-8 -*-

import json
import logging
import werkzeug
from datetime import datetime
from math import ceil

from odoo import fields, http, SUPERUSER_ID
from odoo.http import request
from odoo.tools import ustr

_logger = logging.getLogger(__name__)


class AmwaySurveyWebsiteSurvey(http.Controller):

    @http.route(['/survey/back/<string:token>'], type='http', auth="user", website=True)
    def back(self, token, **post):
        _logger.debug('Incoming data: %s', post)
        id = request.env['survey.user_input'].sudo().search([('token', '=', token)], limit=1).id
        return request.redirect('/web#view_type=form&model=survey.user_input&id=%s&action=survey.action_survey_user_input' % (id)) #request.render('web#view_type=form&model=survey.survey&id=1&action=survey.action_survey_form')
