# -*- coding: utf-8 -*-
{
    'name': "Amway (Umfrage)",

    'summary': """
        Umfrage-Erweiterung für Amway-Vertriebspartner
    """,

    'description': """
        Beinhaltet alle benötigten Erweiterungen um Messungen zu erfassen
    """,

    'author': "Gnehm Informatik GmbH",
    'website': "http://www.gnehm-informatik.ch",

    'application': True,
    'category': 'Specific Industry Applications',
    'version': '0.1',

    'depends': [
        'base',
        'survey',
    ],

    'data': [
        'views/survey.xml',
        'views/res_partner.xml',
    ],
}