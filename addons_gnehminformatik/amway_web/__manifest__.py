# -*- coding: utf-8 -*-
{
    'name': "Amway (Web)",

    'summary': """
        Web-Erweiterung für Amway-Vertriebspartner
    """,

    'description': """
        Beinhaltet alle benötigten Erweiterungen inkl. Web für Amway-Vertriebspartner
    """,

    'author': "Gnehm Informatik GmbH",
    'website': "http://www.gnehm-informatik.ch",

    'application': True,
    'category': 'Specific Industry Applications',
    'version': '0.1',

    'depends': [
        'amway_base',
        'website_blog',
        'website_sale',
    ],

    'data': [
        'views/product_template.xml',
        #'security/ir.model.access.csv',
    ],

    'demo': [
        'demo/demo.xml',
    ],
}