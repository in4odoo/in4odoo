# -*- coding: utf-8 -*-

from odoo import models, fields, api


class AmwayWebProductTemplate(models.Model):

    _inherit = "product.template"

    description_full = fields.Text('Full Description', translate=True, help="A description of the Product that you want to display on the web.")