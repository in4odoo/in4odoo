# -*- coding: utf-8 -*-

from odoo import models, fields, api

import time


sync_direction = [
    ('download', 'Download'),
    ('upload', 'Upload'),
    ('both', 'Both'),
]

field_selection = [
    ('all', 'Synchronize all fields except the fields from the list'),
    ('none', 'Synchronize only fields from the list'),
]


class DBSyncDatabase(models.Model):

    _name = 'db_sync.database'
    _description = 'Database to synchronize'

    name = fields.Char(string='Name', required=True)
    db = fields.Char(string='Database', required=True)
    url = fields.Char(string='URL', required=True, default='http://localhost:8069')
    login = fields.Char(string='User Name', required=True)
    password = fields.Char(string='Password', required=True)
    table_ids = fields.One2many('db_sync.table', 'database_id', string='Tables', copy=True)

    @api.multi
    def write(self, vals):
        result = super(DBSyncDatabase, self).write(vals)

        for database_id in self:
            for table_id in database_id.table_ids:
                table_id.display_name = '{0} ({1})'.format(table_id.name, table_id.database_id.name)

        return result


class DBSyncTable(models.Model):

    _name = 'db_sync.table'
    _description = 'Table to synchronize'
    _order = 'sequence, database_id'

    name = fields.Char(string='Name', required=True)
    display_name = fields.Char(compute='compute_display_name', store=True)
    active = fields.Boolean(string='Active', default=True)
    database_id = fields.Many2one('db_sync.database', string='Database', required=True, ondelete='cascade')
    model_id = fields.Many2one('ir.model', string='Model', required=True, ondelete='cascade')
    direction = fields.Selection(sync_direction, string='Direction', required=True, default='upload')
    sequence = fields.Integer(string='Sequence')
    delete_records = fields.Boolean(string='Delete Records', default=False)
    delete_multiple = fields.Boolean(string='Delete Multiple', default=False)
    domain = fields.Char(string='Domain', required=True, default='[]')

    field_selection = fields.Selection(field_selection, string='Selection', required=True, default='all')
    field_ids = fields.One2many('db_sync.table.field', 'table_id', string='Fields', copy=True)

    synchronize_date = fields.Datetime(string='Last Synchronization', copy=False)
    record_ids = fields.One2many('db_sync.record', 'table_id', string='IDs Affected')

    @api.depends('name', 'database_id')
    def compute_display_name(self):
        for table_id in self:
            table_id.display_name = '{0} ({1})'.format(table_id.name, table_id.database_id.name)

    @api.model
    def get_ids(self, model_id, dt, domain=None, direction=None):
        result = []
        pool = self.env[model_id.model]
        if direction is None:
            direction = {}
        if dt:
            if (model_id.model == 'product.product'):
                w_date = domain + ['|', ('write_date', '>=', dt), ('product_tmpl_id.write_date', '>=', dt)]
            else:
                w_date = domain + [('write_date', '>=', dt)]
            c_date = domain + [('create_date', '>=', dt)]
        else:
            w_date = c_date = domain
        obj_rec = pool.with_context(active_test=False).search(w_date)
        obj_rec += pool.with_context(active_test=False).search(c_date)
        for r in obj_rec.read(['create_date', 'write_date']):
            result.append((r['write_date'] or r['create_date'], r['id'], direction.get('direction', 'download')))

        return result

    @api.model
    def del_remote_ids(self, pool_dest, table_id):
        pool_src = self.env[table_id.model_id.model]
        all_dest_ids = pool_dest.get(table_id.model_id.model)
        if (all_dest_ids):
            record_ids = self.env['db_sync.record'].search([('table_id', '=', table_id.id)])
            for record_id in record_ids:
                obj_rec = pool_src.with_context(active_test=False).search([('id', '=', record_id.local_id)])
                if (not obj_rec):
                    dest_id = all_dest_ids.search([('id', '=', record_id.remote_id)], limit=1)
                    if (dest_id):
                        all_dest_ids.unlink([dest_id[0]])
                    record_id.unlink()


class DBSyncTableField(models.Model):

    _name = 'db_sync.table.field'
    _description = 'Field to synchronize'

    name = fields.Char(string='Name', required=True)
    table_id = fields.Many2one('db_sync.table', string='Table', required=True, ondelete='cascade')


class DBSyncRecord(models.Model):

    _name = 'db_sync.record'
    _description = 'Record'
    _order = 'name desc'

    name = fields.Datetime(string='Date', required=True, default=lambda *args: time.strftime('%Y-%m-%d %H:%M:%S'))
    table_id = fields.Many2one('db_sync.table', string='Table', ondelete='cascade')
    local_id = fields.Integer(string='Local ID', readonly=True)
    remote_id = fields.Integer(string='Remote ID', readonly=True)


class DBSyncRequest(models.Model):

    _name = 'db_sync.request'
    _description = 'Request'
    _order = 'date desc'

    name = fields.Char('Name', required=True)
    date = fields.Datetime('Date')
    body = fields.Text('Request')
