# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.exceptions import Warning

import time
import threading
from xmlrpc.client import ServerProxy

import logging
_logger = logging.getLogger(__name__)


class RPCProxyOne(object):

    def __init__(self, database_id, ressource):
        self.database_id = database_id
        local_url = '%s/xmlrpc/common' % (database_id.url)
        rpc = ServerProxy(local_url)
        self.uid = rpc.login(database_id.db, database_id.login, database_id.password)
        local_url = '%s/xmlrpc/object' % (database_id.url)
        self.rpc = ServerProxy(local_url)
        self.ressource = ressource

    def __getattr__(self, name):
        return lambda *args, **kwargs: self.rpc.execute(self.database_id.db, self.uid, self.database_id.password, self.ressource, name, *args)


class RPCProxy(object):

    def __init__(self, database_id):
        self.database_id = database_id

    def get(self, ressource):
        return RPCProxyOne(self.database_id, ressource)


class DBSyncWizard(models.TransientModel):

    _name = 'db_sync.wizard'

    database_id = fields.Many2one('db_sync.database', string='Database', required=True, ondelete='cascade')
    user_id = fields.Many2one('res.users', string='Send Result To', default=lambda self: self.env.user, ondelete='cascade')
    report = []
    report_total = 0
    report_create = 0
    report_write = 0

    @api.model
    def synchronize(self, database_id, table_id):
        pool = self
        pool1 = RPCProxy(database_id)
        pool2 = pool

        sync_ids = []
        dt = table_id.synchronize_date
        if table_id.direction in ('download', 'both'):
            module = pool1.get('ir.module.module')
            module_id = module.search([('name', 'ilike', 'db_sync'), ('state', '=', 'installed')])
            if not module_id:
                raise Warning(_('If your Direction is "Download" or "Both", please install the "DB-Sync"-Module in Target-Database!'))
            sync_ids = pool1.get('db_sync.table').get_ids(table_id.model_id, dt, eval(table_id.domain), {'direction': 'download'})
        if table_id.direction in ('upload', 'both'):
            # TODO 'delete_records' for download
            if (table_id.delete_records):
                pool2.env['db_sync.table'].del_remote_ids(pool1, table_id)
            _logger.debug('Getting ids to synchronize [%s] (%s)', table_id.synchronize_date, table_id.domain)
            sync_ids += pool2.env['db_sync.table'].get_ids(table_id.model_id, dt, eval(table_id.domain), {'direction': 'upload'})

        sorted(sync_ids, key=lambda x: str(x[0]))
        for dt, id, direction in sync_ids:
            destination_inverted = False
            if direction == 'download':
                pool_src = pool1
                pool_dest = pool2
            else:
                pool_src = pool2
                pool_dest = pool1
                destination_inverted = True

            fields = False
            if table_id.model_id.model == 'crm.case.history':
                fields = ['email', 'description', 'log_id']
            if not destination_inverted:
                value = pool_src.get(table_id.model_id.model).read([id], fields)[0]
            else:
                pool = pool_src.env[table_id.model_id.model]
                value = pool.browse([id]).read(fields)[0]
            if 'display_name' in value:
                del value['display_name']
            if 'create_date' in value:
                del value['create_date']
            if 'write_date' in value:
                del value['write_date']
            for key, val in value.items():
                if isinstance(val, tuple):
                    value.update({key: val[0]})

            value, translated_fields = self.data_transform(pool_src, pool_dest, table_id, value, direction, destination_inverted)
            if (table_id.field_selection == 'all'):
                for field in table_id.field_ids:
                    if field.name in value:
                        del value[field.name]
            else:
                del_value = []
                for val in value:
                    i = 0
                    found = False
                    while ((not found) and (i < len(table_id.field_ids))):
                        field = table_id.field_ids[i]
                        if (field.name == val):
                            found = True
                        i += 1
                    if (not found):
                        del_value.append(val)
                for val in del_value:
                    del value[val]

            id2 = self.get_id(table_id.id, id, direction)
            if (not id2):
                #if not destination_inverted:
                # TODO 'id_new' from ir.model.data (analog 'else')
                #else:
                if (destination_inverted):
                    model_data_id_src = pool_src.env['ir.model.data'].search([
                        ('res_id', '=', id),
                        ('model', '=', table_id.model_id.model),
                    ], limit=1)
                    if (model_data_id_src):
                        model_data_ids_dest = pool_dest.get('ir.model.data')
                        if (model_data_ids_dest):
                            model_data_id_dest = model_data_ids_dest.search([
                                ('name', '=', model_data_id_src.name),
                                ('model', '=', table_id.model_id.model),
                            ], limit=1)
                            if (model_data_id_dest):
                                model_data_fields_dest = model_data_ids_dest.read([model_data_id_dest[0]], fields)
                                id2 = model_data_fields_dest[0]['res_id']
                                self.env['db_sync.record'].create({
                                    'table_id': table_id.id,
                                    'local_id': (direction == 'upload') and id or id2,
                                    'remote_id': (direction == 'download') and id or id2
                                })
            if id2:
                _logger.debug('Updating record %s [%d]', table_id.model_id.name, id2)
                if not destination_inverted:
                    pool = pool_dest.env[table_id.model_id.model]
                    pool.browse([id2]).write(value)
                    # TODO Translated Fields (analog 'else')
                else:
                    try:
                        pool_dest.get(table_id.model_id.model).write([id2], value)

                        translation_ids = pool_dest.get('ir.translation')
                        if (translation_ids):
                            for translated_field in translated_fields:
                                if translated_field in value:
                                    translation_name = table_id.model_id.model + ',' + translated_field
                                    translation_id = translation_ids.search([('name', '=', translation_name), ('res_id', '=', id2)], limit=1)
                                    if (translation_id):
                                        translation_ids.write([translation_id[0]], {'value': value[translated_field]})

                        model_data_id_src = pool_src.env['ir.model.data'].search([
                            ('res_id', '=', id),
                            ('model', '=', table_id.model_id.model),
                        ], limit=1)
                        if (model_data_id_src):
                            model_data_ids_dest = pool_dest.get('ir.model.data')
                            if (model_data_ids_dest):
                                model_data_id_dest = model_data_ids_dest.search([
                                    ('res_id', '=', id2),
                                    ('model', '=', table_id.model_id.model),
                                ], limit=1)
                                if (model_data_id_dest):
                                    model_data_ids_dest.write([model_data_id_dest[0]], {
                                        'name': model_data_id_src.name,
                                        'module': model_data_id_src.module,
                                    })
                                else:
                                    model_data_ids_dest.create({
                                        'res_id': id2,
                                        'name': model_data_id_src.name,
                                        'model': table_id.model_id.model,
                                        'module': model_data_id_src.module,
                                    })
                    except:
                        _logger.warning('Record "%s" on table "%s" not found.', id, table_id.model_id.model)

                self.report_total += 1
                self.report_write += 1
            else:
                _logger.debug('Creating record %s', table_id.model_id.name)
                if not destination_inverted:
                    id_new = pool_dest.env[table_id.model_id.model].create(value).id
                else:
                    id_new = pool_dest.get(table_id.model_id.model).create(value)

                    model_data_id_src = pool_src.env['ir.model.data'].search([
                        ('res_id', '=', id),
                        ('model', '=', table_id.model_id.model),
                    ], limit=1)
                    if (model_data_id_src):
                        model_data_ids_dest = pool_dest.get('ir.model.data')
                        if (model_data_ids_dest):
                            model_data_ids_dest.create({
                                'res_id': id_new,
                                'name': model_data_id_src.name,
                                'model': table_id.model_id.model,
                                'module': model_data_id_src.module,
                            })

                self.env['db_sync.record'].create({
                    'table_id': table_id.id,
                    'local_id': (direction == 'upload') and id or id_new,
                    'remote_id': (direction == 'download') and id or id_new
                })
                if (table_id.model_id.model == 'product.product'):
                    #if not destination_inverted:
                    # TODO analog 'else'
                    #else:
                    if (destination_inverted):
                        template_table_id = pool_src.env['db_sync.table'].search([
                            ('database_id', '=', self.database_id.id),
                            ('model_id.model', '=', 'product.template'),
                        ], limit=1)

                        id_template = None
                        product_id_src = pool_src.env['product.product'].search([('id', '=', id)], limit=1)
                        if (product_id_src):
                            id_template = product_id_src.product_tmpl_id.id

                        id_template2 = None
                        product_ids_dest = pool_dest.get('product.product')
                        if (product_ids_dest):
                            product_id_dest = product_ids_dest.search([('id', '=', id_new)], limit=1)
                            if (product_id_dest):
                                product_fields_dest = product_ids_dest.read([product_id_dest[0]], fields)
                                id_template2 = product_fields_dest[0]['product_tmpl_id'][0]

                        if ((template_table_id) and (id_template) and (id_template2)):
                            record_id = self.env['db_sync.record'].search([
                                ('table_id', '=', template_table_id.id),
                                ('local_id', '=', id_template),
                                ('remote_id', '=', id_template2),
                            ])
                            if (not record_id):
                                self.env['db_sync.record'].create({
                                    'table_id': template_table_id.id,
                                    'local_id': id_template,
                                    'remote_id': id_template2
                                })

                self.report_total += 1
                self.report_create += 1

        if (    (table_id.delete_multiple)
            and (table_id.direction in ('upload', 'both'))\
            and (table_id.model_id.model == 'product.attribute.line')
        ):
            pool_src = self.env[table_id.model_id.model]
            pool_dest = pool1
            all_dest_ids = pool_dest.get(table_id.model_id.model)
            if (all_dest_ids):
                record_ids = self.env['db_sync.record'].search([('table_id', '=', table_id.id)])
                for record_id in record_ids:
                    src_id = pool_src.with_context(active_test=False).search([('id', '=', record_id.local_id)])
                    if (src_id):
                        attribute_id = self.env['db_sync.record'].search([
                            ('table_id.database_id', '=', table_id.database_id.id),
                            ('table_id.model_id.model', '=', 'product.attribute'),
                            ('local_id', '=', src_id.attribute_id.id),
                        ], limit=1)
                        product_tmpl_id = self.env['db_sync.record'].search([
                            ('table_id.database_id', '=', table_id.database_id.id),
                            ('table_id.model_id.model', '=', 'product.template'),
                            ('local_id', '=', src_id.product_tmpl_id.id),
                        ], limit=1)
                        dest_ids = all_dest_ids.search([
                            ('id', '!=', record_id.remote_id),
                            ('attribute_id', '=', attribute_id.remote_id),
                            ('product_tmpl_id', '=', product_tmpl_id.remote_id),
                        ])
                        for dest_id in dest_ids:
                            all_dest_ids.unlink([dest_id])
            table_id.delete_multiple = False

        return True

    @api.model
    def get_id(self, id_table, id, direction):
        record_ids = self.env['db_sync.record']
        field_src = (direction == 'upload') and 'local_id' or 'remote_id'
        field_dest = (direction == 'download') and 'local_id' or 'remote_id'

        result = False
        record_id = record_ids.search([('table_id', '=', id_table), (field_src, '=', id)])
        if record_id:
            result = record_ids.browse([record_id[0].id]).read([field_dest])
            if result:
                result = result[0][field_dest]
        return result

    @api.model
    def get_id_by_model(self, model_name, id, direction):
        record_ids = self.env['db_sync.record']
        field_src = (direction == 'upload') and 'local_id' or 'remote_id'
        field_dest = (direction == 'download') and 'local_id' or 'remote_id'

        result = False
        record_id = record_ids.search([('table_id.model_id.model', '=', model_name), (field_src, '=', id)])
        if record_id:
            result = record_ids.browse([record_id[0].id]).read([field_dest])
            if result:
                result = result[0][field_dest]
        return result

    @api.model
    def relation_transform(self, pool_src, pool_dest, table_id, model_name, res_id, direction, destination_inverted):
        result = False
        if not res_id:
            return result
        _logger.debug('Relation transform')
        self._cr.execute('''
               SELECT tbl.id
                 FROM db_sync_table tbl
            LEFT JOIN ir_model mdl ON mdl.id = tbl.model_id
                WHERE tbl.active
                  AND tbl.database_id = %s
                  AND mdl.model = %s
        ''', (self.database_id.id, model_name,))
        table_ids = self._cr.fetchone()
        if table_ids:
            result = self.get_id(table_ids[0], res_id, direction)
            _logger.debug('Relation already synchronized. Getting id %s', result)
        else:
            _logger.debug('Relation not synchronized. Searching by name_get and name_search')
            if not destination_inverted:
                names = pool_src.get(model_name).name_get([res_id])[0][1]
                res = pool_dest.env[model_name].name_search(names, [], 'like')
            else:
                pool = pool_src.env[model_name]
                names = pool.browse([res_id]).name_get()[0][1]
                res = pool_dest.get(model_name).name_search(names, [], 'like')
            _logger.debug('name_get in src: %s', names)
            _logger.debug('name_search in dest: %s', res)
            if res:
                result = res[0][0]
            else:
                _logger.warning('Record "%s" on relation "%s" not found, set to null.', names, model_name)
                _logger.warning('You should consider synchronize this model "%s"', model_name)
                self.report.append('WARNING: Record "%s" on relation "%s" on table "%s" not found, set to null.' % (names, model_name, table_id.model_id.model))
        return result

    @api.model
    def data_transform(self, pool_src, pool_dest, table_id, data, direction=None, destination_inverted=False):
        if direction is None:
            direction = {}
        if not destination_inverted:
            fields = pool_src.get(table_id.model_id.model).fields_get()
        else:
            fields = pool_src.env[table_id.model_id.model].fields_get()

        field_list = []
        for field_id in table_id.field_ids:
            field_list.append(field_id.name)

        _logger.debug('Transforming data')
        translated_fields = []
        for f in fields:
            if (   ((table_id.field_selection == 'all') and (not (f in field_list)))
                or ((table_id.field_selection == 'none') and (f in field_list))
            ):
                ftype = fields[f]['type']
                if ftype.lower() in ('function', 'one2many', 'one2one'):
                    _logger.debug('Field %s of type %s, discarded.', f, ftype)
                    del data[f]
                elif ftype.lower() == 'many2one':
                    _logger.debug('Field %s is many2one', f)
                    if (isinstance(data[f], list)) and data[f]:
                        fdata = data[f][0]
                    else:
                        fdata = data[f]
                    df = self.relation_transform(pool_src, pool_dest, table_id, fields[f]['relation'], fdata, direction, destination_inverted)
                    data[f] = df
                    if not data[f]:
                        del data[f]
                elif ftype.lower() == 'many2many':
                    res = map(lambda x: self.relation_transform(pool_src, pool_dest, table_id, fields[f]['relation'], x, direction, destination_inverted), data[f])
                    data[f] = [(6, 0, [x for x in res if x])]

                field_translate = fields[f].get('translate')
                if (field_translate):
                    translated_fields.append(f)

        del data['id']

        return data, translated_fields

    @api.multi
    def upload_download(self):
        self.report = []
        start_date = time.strftime('%Y-%m-%d, %Hh %Mm %Ss')
        wizard_id = self.browse(self.ids)[0]
        database_id = self.env['db_sync.database'].browse(wizard_id.database_id.id)
        for table_id in database_id.table_ids:
            _logger.debug('Start synchro of %s', table_id.name)
            dt = time.strftime('%Y-%m-%d %H:%M:%S')
            self.synchronize(database_id, table_id)
            if table_id.direction == 'both':
                time.sleep(1)
                dt = time.strftime('%Y-%m-%d %H:%M:%S')
            table_id.write({'synchronize_date': dt})
        end_date = time.strftime('%Y-%m-%d, %Hh %Mm %Ss')

        # Creating db_sync.request for summary results
        if wizard_id.user_id:
            request = self.env['db_sync.request']
            if not self.report:
                self.report.append('No exception.')
            summary = '''
Synchronization started: %s
Synchronization finished: %s

Synchronized records: %d
Records updated: %d
Records created: %d

Exceptions:
            ''' % (start_date, end_date, self.report_total, self.report_write, self.report_create)
            summary = summary.strip() + '\n'
            summary += '\n'.join(self.report)
            request.create({
                'name': 'Synchronization report',
                'date': time.strftime('%Y-%m-%d, %H:%M:%S'),
                'body': summary,
            })
            return {}

    @api.multi
    def upload_download_multi_thread(self):
        threaded_synchronization = threading.Thread(target=self.upload_download())
        threaded_synchronization.run()
        id2 = self.env.ref('db_sync.wizard_form_finish').id
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'db_sync.wizard',
            'views': [(id2, 'form')],
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }
