# -*- coding: utf-8 -*-
{
    'name': "DB-Sync",

    'summary': """
        Database Synchronization
    """,

    'description': """
        2-way-synchronization of tables in different databases
    """,

    'author': "Gnehm Informatik GmbH",
    'website': "https://www.gnehm-informatik.ch",

    'application': True,
    'category': 'Tools',
    'version': '0.1',

    'depends': [
        'base',
    ],

    'data': [
        'views/db_sync.xml',
        'views/db_sync_wizard.xml',
        'security/ir.model.access.csv',
    ],
}