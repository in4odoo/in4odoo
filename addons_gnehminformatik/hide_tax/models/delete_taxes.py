# -*- coding: utf-8 -*-

from odoo import models, fields, api


class HideTaxDeleteTaxes(models.TransientModel):

    _name = 'hide_tax.delete_taxes'

    @api.multi
    def delete_taxes(self):
        template_ids = self.env['product.template'].with_context(active_test=False).search([])
        for template_id in template_ids:
            for tax_id in template_id.taxes_id:
                template_id.write({'taxes_id': [(3, tax_id.id)]})
            for supplier_tax_id in template_id.supplier_taxes_id:
                template_id.write({'supplier_taxes_id': [(3, supplier_tax_id.id)]})

        product_ids = self.env['product.product'].with_context(active_test=False).search([])
        for product_id in product_ids:
            for tax_id in product_id.taxes_id:
                product_id.write({'taxes_id': [(3, tax_id.id)]})
            for supplier_tax_id in product_id.supplier_taxes_id:
                product_id.write({'supplier_taxes_id': [(3, supplier_tax_id.id)]})
