# -*- coding: utf-8 -*-
{
    'name': 'Hide Tax',

    'summary': """
        Hide tax on invoices
    """,

    'description': """
        Hide tax on invoices
    """,

    'author': 'Gnehm Informatik GmbH',
    'website': 'https://www.gnehm-informatik.ch',

    'application': True,
    'category': 'Tools',
    'version': '0.1',

    'depends': [
        'base',
        'sale_management',
    ],

    'data': [
        'views/account_invoice.xml',
        'views/delete_taxes.xml',
        'views/res_company.xml',
    ],
}