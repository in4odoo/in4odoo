# -*- coding: utf-8 -*-

import logging

from odoo import http
from odoo.http import request
from odoo.tools.translate import _
from odoo.tools.misc import DEFAULT_SERVER_DATE_FORMAT
from datetime import datetime
from dateutil.relativedelta import relativedelta

_logger = logging.getLogger(__name__)
# _logger.info()
# _logger.warning()


class StalderTiefbauAG(http.Controller):

    @http.route('/stalder_tiefbau_ag/timesheet', type='http', auth='user', website=True)
    def timesheet(self, **kw):
        user_id = request.session.uid
        is_timesheet_admin = request.env['res.users'].browse(user_id).has_group('base.group_user')

        users = None
        if is_timesheet_admin:
            domain = []
            users = request.env['res.users'].sudo().search(domain)

        return request.render('stalder_tiefbau_ag.timesheet', {'users': users,
                                                               'is_timesheet_admin': is_timesheet_admin,
                                                              })


    @http.route('/stalder_tiefbau_ag/timesheet/load', type='json', auth='user', website=True)
    def timesheet_load(self, **kw):
        user_id = request.session.uid
        is_timesheet_admin = request.env['res.users'].browse(user_id).has_group('base.group_user')
        if is_timesheet_admin:
            user_id = kw.get('user')
        year_selected = kw.get('year')
        month_selected = kw.get('month')
        if user_id and year_selected and month_selected:
            lots = []
            others = []
            remark = ''
            current_month_is_closed = False
            prev_month_is_closed = True

            prev_year = int(year_selected)
            prev_month = int(month_selected) - 1
            if prev_month <= 0:
                prev_year = int(prev_year) - 1
                prev_month = 12
            domain = [('user_id', '=', user_id), ('year', '=', prev_year), ('month', '=', prev_month)]
            timesheet_prev_month = request.env['stalder_tiefbau_ag.timesheet_month'].sudo().search(domain, limit=1)
            if timesheet_prev_month:
                prev_month_is_closed = timesheet_prev_month.closed

            domain = [('user_id', '=', user_id), ('year', '=', year_selected), ('month', '=', month_selected)]
            timesheet_month = request.env['stalder_tiefbau_ag.timesheet_month'].sudo().search(domain, limit=1)
            if timesheet_month:
                remark = timesheet_month.remark
                current_month_is_closed = timesheet_month.closed
                for timesheet_lot in timesheet_month.timesheet_lot_ids:
                    days = []
                    for timesheet_day in timesheet_lot.timesheet_day_ids:
                        day = [
                            timesheet_day.day,
                            timesheet_day.work_duration,
                            timesheet_day.prepare_duration,
                            timesheet_day.travel_duration,
                            timesheet_day.food_charge,
                        ]
                        days.append(day)
                    lot = [timesheet_lot.description, timesheet_lot.number, days];
                    lots.append(lot)

                for timesheet_other in timesheet_month.timesheet_other_ids:
                    other = [timesheet_other.day, timesheet_other.type, timesheet_other.duration]
                    others.append(other)

            date_from = datetime(year=int(year_selected), month=int(month_selected), day=1)
            date_to = date_from + relativedelta(months=1) -  relativedelta(days=1)
            domain = ['&', '&', ('date', '>=', datetime.strftime(date_from, DEFAULT_SERVER_DATE_FORMAT)),
                                ('date', '<=', datetime.strftime(date_to, DEFAULT_SERVER_DATE_FORMAT)),
                           '|', ('user_ids', 'in', user_id),
                                ('user_ids', '=', False),
                     ]
            timesheet_templates = request.env['stalder_tiefbau_ag.timesheet_template'].sudo().search(domain)
            for timesheet_template in timesheet_templates:
                day = datetime.strptime(timesheet_template.date, DEFAULT_SERVER_DATE_FORMAT).day
                found = False
                for other in others:
                    if other[0] == day:
                        found = True
                if not found:
                    template = [day, timesheet_template.type, timesheet_template.duration]
                    others.append(template)

            return [lots, others, remark, is_timesheet_admin, current_month_is_closed, prev_month_is_closed]

        return False


    @http.route('/stalder_tiefbau_ag/timesheet/save', type='json', auth='user', website=True)
    def timesheet_save(self, **kw):
        user_id = request.session.uid
        is_timesheet_admin = request.env['res.users'].browse(user_id).has_group('base.group_user')
        if (is_timesheet_admin):
            user_id = kw.get('user')
        year_selected = kw.get('year')
        month_selected = kw.get('month')
        lots = kw.get('lots')
        others = kw.get('others')
        remark = kw.get('remark')
        if (     user_id
             and year_selected
             and month_selected
             and (lots or others)
        ):
            val = None
            lot_nr = 0
            day_in_month = 0
            input_type = ''
            lot_description = ''
            others_count = 0
            lots_count = 0

            request.cr.autocommit(False)
            try:
                domain = [('user_id', '=', user_id), ('year', '=', year_selected), ('month', '=', month_selected)]
                timesheet_month = request.env['stalder_tiefbau_ag.timesheet_month'].sudo().search(domain, limit=1)
                if timesheet_month:
                    timesheet_month.timesheet_other_ids.unlink()
                    timesheet_month.timesheet_lot_ids.unlink()
                else:
                    timesheet_month = request.env['stalder_tiefbau_ag.timesheet_month'].sudo().create({
                        'user_id': user_id,
                        'year': year_selected,
                        'month': month_selected,
                    })
                timesheet_month.remark = remark

                if others:
                    for other in others:
                        day_in_month = day_in_month + 1

                        other_type = other[0]
                        other_duration = other[1]

                        val = None
                        input_type = ''
                        if other_duration:
                            val = other_duration
                            input_type = 'x'
                            other_duration = round(float(other_duration) * 4) / 4

                        if (    (other_type and (len(other_type) > 0))
                             or (other_duration and (float(other_duration) > 0))
                        ):
                            timesheet_month.timesheet_other_ids.sudo().create({
                                'timesheet_month_id': timesheet_month.id,
                                'day': day_in_month,
                                'type': other_type,
                                'duration': other_duration,
                            })
                            others_count += 1

                if lots:
                    for lot in lots:
                        lot_nr = lot_nr + 1
                        lot_description = lot[1]
                        lot_number = lot[2]
                        timesheet_lot = timesheet_month.timesheet_lot_ids.sudo().create({
                            'timesheet_month_id': timesheet_month.id,
                            'sequence': lot[0],
                            'description': lot_description,
                            'number': lot_number,
                        })
                        lots_count += 1

                        day_in_month = 0
                        for day in lot[3]:
                            day_in_month = day_in_month + 1

                            work_duration = day[0]
                            prepare_duration = day[1]
                            travel_duration = day[2]
                            food_charge = day[3]

                            val = None
                            input_type = ''
                            if work_duration:
                                val = work_duration
                                input_type = 'a'
                                work_duration = round(float(work_duration) * 4) / 4
                            if prepare_duration:
                                val = prepare_duration
                                input_type = 'v'
                                prepare_duration = round(float(prepare_duration) * 4) / 4
                            if travel_duration:
                                val = travel_duration
                                input_type = 'r'

                            if (    (work_duration and (float(work_duration) > 0))
                                 or (prepare_duration and (float(prepare_duration) > 0))
                                 or (travel_duration and (int(travel_duration) > 0))
                                 or (food_charge)
                            ):
                                timesheet_lot.timesheet_day_ids.sudo().create({
                                    'timesheet_lot_id': timesheet_lot.id,
                                    'day': day_in_month,
                                    'work_duration': work_duration,
                                    'prepare_duration': prepare_duration,
                                    'travel_duration': travel_duration,
                                    'food_charge': food_charge,
                            })
                if (others_count == 0) and (lots_count == 0):
                    timesheet_month.unlink()
                request.cr.commit()
            except Exception as e:
                request.cr.rollback()
                return [
                    lot_nr - 1,
                    day_in_month - 1,
                    input_type,
                    _('"{0}" is not a correct value (Lot: {1}, Day {2})').format(
                        val,
                        lot_description,
                        str(day_in_month)
                    )
                ]
                pass

        return ''


    @http.route('/stalder_tiefbau_ag/timesheet/close', type='json', auth='user', website=True)
    def timesheet_close(self, **kw):
        user_id = request.session.uid
        is_timesheet_admin = request.env['res.users'].browse(user_id).has_group('base.group_user')
        if is_timesheet_admin:
            user_id = kw.get('user')
        year_selected = kw.get('year')
        month_selected = kw.get('month')
        if user_id and year_selected and month_selected:
            domain = [('user_id', '=', user_id), ('year', '=', year_selected), ('month', '=', month_selected)]
            timesheet_month = request.env['stalder_tiefbau_ag.timesheet_month'].sudo().search(domain, limit=1)
            if timesheet_month:
                timesheet_month.closed = True

        return ''
