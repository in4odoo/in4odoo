odoo.define('stalder_tiefbau_ag.website_timesheet_system', function (require) {
  'use strict';

  //console.info('Test');

  var core = require('web.core');
  var ajax = require('web.ajax');

  var save_done = true;
  var do_reload = true;
  var load_data_from_user = null;
  var load_data_from_year = null;
  var load_data_from_month = null;

  var recalcTimesheet = function(row, type, day) {
    var tot_day_a = 0;
    var tot_day_v = 0;
    var tot_day_r = 0;
    var tot_day_m = 0;
    var tot_day_x = 0;
    var tot_week = 0;
    var tot_month = 0;
    var time_controls = document.getElementsByClassName('timesheet_time_text');
    for (var i = 0; i < time_controls.length; ++i) {
      var item = time_controls[i];
      var item_type = item.id.substr(item.id.length - 1, 1)
      if ((item_type != 'm') || (item.checked)) {
        if (    (row.length > 0)
             && (item_type == type)
             && (item.id.substr(3, row.length) == row)
             && (item.id.substr(3 + row.length, 1) == '_')
           )
        {
          var val = item.value;
          if ((!isNaN(val)) && (val >= 0) && ((val < 100) || ((item_type == 'r') && (val < 1000)))) {
            tot_month = tot_month + Number(val);
            if (item.parentElement.style.getPropertyValue('display') != 'none') {
              tot_week = tot_week + Number(val);
            }
          }
        }
        if (    (day.length > 0)
             && (item.id.substr(item.id.length - 3 - day.length, 1) == '_')
             && (item.id.substr(item.id.length - 2 - day.length, day.length) == day)
           )
        {
          var val = item.value;
          if ((!isNaN(val)) && (val >= 0) && ((val < 100) || ((item_type == 'r') && (val < 1000)))) {
            if (item_type == 'a') {
              tot_day_a = tot_day_a + Number(val);
            }
            if (item_type == 'v') {
              tot_day_v = tot_day_v + Number(val);
            }
            if (item_type == 'r') {
              tot_day_r = tot_day_r + Number(val);
            }
            if (item_type == 'm') {
              tot_day_m = tot_day_m + Number(val);
            }
            if (item_type == 'x') {
              tot_day_x = tot_day_x + Number(val);
            }
          }
        }
      }
    }
    if (row.length > 0) {
      if (tot_week == 0) {
        document.getElementById('tw_' + row + '_' + type).value = '';
      } else {
        document.getElementById('tw_' + row + '_' + type).value = tot_week;
      }
      if (tot_month == 0) {
        document.getElementById('tm_' + row + '_' + type).value = '';
      } else {
        document.getElementById('tm_' + row + '_' + type).value = tot_month;
      }
    }
    if (day.length > 0) {
      if (tot_day_a + tot_day_x == 0) {
        document.getElementById('tt_' + day + '_a').value = '';
      } else {
        document.getElementById('tt_' + day + '_a').value = tot_day_a + tot_day_x;
      }
      if (tot_day_v == 0) {
        document.getElementById('tt_' + day + '_v').value = '';
      } else {
        document.getElementById('tt_' + day + '_v').value = tot_day_v;
      }
      if (tot_day_a + tot_day_v + tot_day_x == 0) {
        document.getElementById('tt_' + day + '_f').value = '';
      } else {
        document.getElementById('tt_' + day + '_f').value = tot_day_a + tot_day_v + tot_day_x;
      }
      if (tot_day_r == 0) {
        document.getElementById('tt_' + day + '_r').value = '';
      } else {
        document.getElementById('tt_' + day + '_r').value = tot_day_r;
      }
      if (tot_day_m == 0) {
        document.getElementById('tt_' + day + '_m').value = '';
      } else {
        document.getElementById('tt_' + day + '_m').value = tot_day_m;
      }
    }

    var first_sunnday = 7;
    var first_date = new Date(document.getElementById('date_selected').value.substr(0, 8) + '01');
    if (first_date.getDay() > 0) {
      first_sunnday = first_date.getDay();
    }

    var week_1_tot = 0;
    var week_2_tot = 0;
    var week_3_tot = 0;
    var week_4_tot = 0;
    var week_5_tot = 0;
    var total_day_controls = document.getElementsByClassName('timesheet_total_day');
    for (var i = 0; i < total_day_controls.length; ++i) {
      var item = total_day_controls[i];
      var val = item.value;
      if (!isNaN(val)) {
        var day = 0;
        if (item.id.substr(item.id.length - 4, 1) == '_') {
          day = Number(item.id.substr(item.id.length - 3, 1)) + 1;
        } else {
          day = Number(item.id.substr(item.id.length - 4, 2)) + 1;
        }
        if (day < 9 - first_sunnday) {
          week_1_tot = week_1_tot + Number(val);
        }
        if ((day >= 9 - first_sunnday) && (day < 16 - first_sunnday)) {
          week_2_tot = week_2_tot + Number(val);
        }
        if ((day >= 16 - first_sunnday) && (day < 23 - first_sunnday)) {
          week_3_tot = week_3_tot + Number(val);
        }
        if ((day >= 23 - first_sunnday) && (day < 30 - first_sunnday)) {
          week_4_tot = week_4_tot + Number(val);
        }
        if (day >= 30 - first_sunnday) {
          week_5_tot = week_5_tot + Number(val);
        }
      }
    }
    for (var i = 0; i < 31; ++i){
      document.getElementById('tt_' + i.toString() + '_w').value = null;
    }
    if (week_1_tot == 0) {
      document.getElementById('tt_' + (7 - first_sunnday).toString() + '_w').value = null;
    } else {
      document.getElementById('tt_' + (7 - first_sunnday).toString() + '_w').value = week_1_tot;
    }
    if (week_2_tot == 0) {
      document.getElementById('tt_' + (14 - first_sunnday).toString() + '_w').value = null;
    } else {
      document.getElementById('tt_' + (14 - first_sunnday).toString() + '_w').value = week_2_tot;
    }
    if (week_3_tot == 0) {
      document.getElementById('tt_' + (21 - first_sunnday).toString() + '_w').value = null;
    } else {
      document.getElementById('tt_' + (21 - first_sunnday).toString() + '_w').value = week_3_tot;
    }
    if (week_4_tot == 0) {
      document.getElementById('tt_' + (28 - first_sunnday).toString() + '_w').value = null;
    } else {
      document.getElementById('tt_' + (28 - first_sunnday).toString() + '_w').value = week_4_tot;
    }
    if (35 - first_sunnday < 31) {
      if (week_5_tot == 0) {
        document.getElementById('tt_' + (35 - first_sunnday).toString() + '_w').value = null;
      } else {
        document.getElementById('tt_' + (35 - first_sunnday).toString() + '_w').value = week_5_tot;
      }
    }

    var week_tot_a = 0;
    var week_tot_v = 0;
    var week_tot_r = 0;
    var week_tot_m = 0;
    var week_tot_x = 0;
    var week_controls = document.getElementsByClassName('timesheet_total_week');
    for (var i = 0; i < week_controls.length; ++i) {
      var item = week_controls[i];
      var val = item.value;
      if (!isNaN(val)) {
        if (item.id.substr(item.id.length - 1, 1) == 'a') {
          week_tot_a = week_tot_a + Number(val);
        }
        if (item.id.substr(item.id.length - 1, 1) == 'v') {
          week_tot_v = week_tot_v + Number(val);
        }
        if (item.id.substr(item.id.length - 1, 1) == 'r') {
          week_tot_r = week_tot_r + Number(val);
        }
        if (item.id.substr(item.id.length - 1, 1) == 'm') {
          week_tot_m = week_tot_m + Number(val);
        }
        if (item.id.substr(item.id.length - 1, 1) == 'x') {
          week_tot_x = week_tot_x + Number(val);
        }
      }
    }
    if (week_tot_a + week_tot_x == 0) {
      document.getElementById('tx_total_week_a').value = null;
    } else {
      document.getElementById('tx_total_week_a').value = week_tot_a + week_tot_x;
    }
    if (week_tot_v == 0) {
      document.getElementById('tx_total_week_v').value = null;
    } else {
      document.getElementById('tx_total_week_v').value = week_tot_v;
    }
    if (week_tot_a + week_tot_v + week_tot_x == 0) {
      document.getElementById('tx_total_week').value = null;
    } else {
      document.getElementById('tx_total_week').value = week_tot_a + week_tot_v + week_tot_x;
    }
    if (week_tot_r == 0) {
      document.getElementById('tx_total_week_r').value = null;
    } else {
      document.getElementById('tx_total_week_r').value = week_tot_r;
    }
    if (week_tot_m == 0) {
      document.getElementById('tx_total_week_m').value = null;
    } else {
      document.getElementById('tx_total_week_m').value = week_tot_m;
    }

    var month_tot_a = 0;
    var month_tot_v = 0;
    var month_tot_r = 0;
    var month_tot_m = 0;
    var month_tot_x = 0;
    var month_controls = document.getElementsByClassName('timesheet_total_month');
    for (var i = 0; i < month_controls.length; ++i) {
      var item = month_controls[i];
      var val = item.value;
      if (!isNaN(val)) {
        if (item.id.substr(item.id.length - 1, 1) == 'a') {
          month_tot_a = month_tot_a + Number(val);
        }
        if (item.id.substr(item.id.length - 1, 1) == 'v') {
          month_tot_v = month_tot_v + Number(val);
        }
        if (item.id.substr(item.id.length - 1, 1) == 'r') {
          month_tot_r = month_tot_r + Number(val);
        }
        if (item.id.substr(item.id.length - 1, 1) == 'm') {
          month_tot_m = month_tot_m + Number(val);
        }
        if (item.id.substr(item.id.length - 1, 1) == 'x') {
          month_tot_x = month_tot_x + Number(val);
        }
      }
    }
    if (month_tot_a + month_tot_x == 0) {
      document.getElementById('tx_total_month_a').value = null;
    } else {
      document.getElementById('tx_total_month_a').value = month_tot_a + month_tot_x;
    }
    if (month_tot_v == 0) {
      document.getElementById('tx_total_month_v').value = null;
    } else {
      document.getElementById('tx_total_month_v').value = month_tot_v;
    }
    if (month_tot_a + month_tot_v + month_tot_x == 0) {
      document.getElementById('tx_total_month').value = null;
    } else {
      document.getElementById('tx_total_month').value = month_tot_a + month_tot_v + month_tot_x;
    }
    if (month_tot_r == 0) {
      document.getElementById('tx_total_month_r').value = null;
    } else {
      document.getElementById('tx_total_month_r').value = month_tot_r;
    }
    if (month_tot_m == 0) {
      document.getElementById('tx_total_month_m').value = null;
    } else {
      document.getElementById('tx_total_month_m').value = month_tot_m;
    }
  }


  var selectionChange = function() {
    var line_count = -1
    var lot_description_controls = document.getElementsByClassName('timesheet_lot_description_input');
    for (var i = 0; i < lot_description_controls.length; ++i) {
      var item = lot_description_controls[i];
      if ((item.value.length > 0) && (line_count < i)) {
        line_count = i;
      }
    }
    for (var i = 0; i < 30; ++i) {
      var line_control = document.getElementById('line_' + i.toString() + '_control');
      if (i <= line_count + 1) {
        line_control.style.setProperty('display', '');
      } else {
        line_control.style.setProperty('display', 'none');
      }

      var input_control_nr = document.getElementById('nr_' + i.toString() + '_a');
      if (i <= line_count) {
          input_control_nr.readOnly = false;
      } else {
          input_control_nr.value = null;
          input_control_nr.readOnly = true;
      }

      for (var j = 0; j < 31; ++j) {
        var input_control_a = document.getElementById('tx_' + i.toString() + '_' + j.toString() + '_a');
        var input_control_v = document.getElementById('tx_' + i.toString() + '_' + j.toString() + '_v');
        var input_control_r = document.getElementById('tx_' + i.toString() + '_' + j.toString() + '_r');
        var input_control_m = document.getElementById('tx_' + i.toString() + '_' + j.toString() + '_m');
        if (i <= line_count) {
          input_control_a.readOnly = false;
          input_control_v.readOnly = false;
          input_control_r.readOnly = false;
          input_control_m.disabled = false;
        } else {
          input_control_a.value = null;
          input_control_v.value = null;
          input_control_r.value = null;
          input_control_m.checked = false;

          input_control_a.readOnly = true;
          input_control_v.readOnly = true;
          input_control_r.readOnly = true;
          input_control_m.disabled = true;
        }
      }
    }

    var sel_date = new Date(document.getElementById('date_selected').value.substr(0, 10));
    var last_date = new Date(document.getElementById('date_selected').value.substr(0, 8) + '01');
    last_date.setHours(12); //Umstellung Sommer-/Winterzeit
    last_date.setMonth(last_date.getMonth() + 1);
    last_date.setDate(last_date.getDate() - 1);
    var day_count = Number(last_date.toISOString().substr(8, 2));

    var start_date = new Date(sel_date);
    if (sel_date.getDay() == 0) {
      start_date.setDate(start_date.getDate() - 6);
    } else {
      start_date.setDate(start_date.getDate() - sel_date.getDay() + 1);
    }
    var start_day = Number(start_date.toISOString().substr(8, 2));

    var end_date = new Date(start_date);
    end_date.setDate(end_date.getDate() + 6);
    var end_day = Number(end_date.toISOString().substr(8, 2));

    var week_is_checked = document.getElementById('time_selection').value == 'week';
    if (week_is_checked) {
      document.getElementById('button_prev_month').style.setProperty('display', 'none');
      document.getElementById('button_prev_week').style.setProperty('display', '');
      document.getElementById('button_next_week').style.setProperty('display', '');
      document.getElementById('button_next_month').style.setProperty('display', 'none');
      document.getElementById('timesheet_container').style.setProperty('width', '624px'); //table_timesheet.width: 594px + 30px
    } else {
      document.getElementById('button_prev_month').style.setProperty('display', '');
      document.getElementById('button_prev_week').style.setProperty('display', 'none');
      document.getElementById('button_next_week').style.setProperty('display', 'none');
      document.getElementById('button_next_month').style.setProperty('display', '');

      var day_delta_width = (40 + 1) * (31 - day_count);
      document.getElementById('timesheet_container').style.setProperty('width', (1552 - day_delta_width).toString() + 'px'); //table_timesheet.width: 1522px + 30px
    }

    var day_controls = document.getElementsByClassName('timesheet_day');
    for (var i = 0; i < day_controls.length; ++i) {
      var item = day_controls[i];
      var day = 0;
      if (item.id.substr(item.id.length - 4, 1) == '_') {
        day = Number(item.id.substr(item.id.length - 3, 1)) + 1;
      } else {
        day = Number(item.id.substr(item.id.length - 4, 2)) + 1;
      }
      if (    (day <= day_count)
           && (    (!week_is_checked)
                || (    (    (day >= start_day)
                          && (day < start_day + 7)
                          && (Number(start_date.toISOString().substr(5, 2)) == Number(sel_date.toISOString().substr(5, 2)))
                        )
                     || (    (day <= end_day)
                          && (day > end_day - 7)
                          && (Number(end_date.toISOString().substr(5, 2)) == Number(sel_date.toISOString().substr(5, 2)))
                        )
                   )
              )
         )
      {
        item.style.setProperty('display', '');
      } else {
        item.style.setProperty('display', 'none');
      }
    }

    var week_controls = document.getElementsByClassName('timesheet_week');
    for (var i = 0; i < week_controls.length; ++i) {
      var item = week_controls[i];
      if (week_is_checked) {
        item.style.setProperty('display', '');
      } else {
        item.style.setProperty('display', 'none');
      }
    }

    for (var i = 0; i < 30; ++i) {
      recalcTimesheet(i.toString(), 'a', '');
      recalcTimesheet(i.toString(), 'v', '');
      recalcTimesheet(i.toString(), 'r', '');
      recalcTimesheet(i.toString(), 'm', '');
    }
    recalcTimesheet('0', 'x', '');
  }


  var dateChange = function() {
    var sel_date = document.getElementById('date_selected').value.substr(0, 8) + '01';

    var sel_year = Number(sel_date.substr(0, 4));
    var sel_month = Number(sel_date.substr(5, 2));
    var display_travel = true;
    if ((sel_year < 2019) || ((sel_year == 2019) && (sel_month < 4))) {
        var display_travel = false;
    }
    var prepare_controls = document.getElementsByClassName('timesheet_display_prepare_duration');
    for (var i = 0; i < prepare_controls.length; ++i) {
      var item = prepare_controls[i];
      if (display_travel == true) {
        item.style.setProperty('display', 'none');
      } else {
        item.style.setProperty('display', '');
      }
    }
    var travel_controls = document.getElementsByClassName('timesheet_display_travel_duration');
    for (var i = 0; i < travel_controls.length; ++i) {
      var item = travel_controls[i];
      if (display_travel == true) {
        item.style.setProperty('display', '');
      } else {
        item.style.setProperty('display', 'none');
      }
    }

    var day_controls = document.getElementsByClassName('timesheet_day_control');
    for (var i = 0; i < day_controls.length; ++i) {
      var item = day_controls[i];
      var day = 0;
      if (item.id.substr(item.id.length - 4, 1) == '_') {
        day = Number(item.id.substr(item.id.length - 3, 1));
      } else {
        day = Number(item.id.substr(item.id.length - 4, 2));
      }
      var other_control = document.getElementById('tx_0_' + day.toString() + '_s');
      switch (other_control.value) {
        case 'holiday':
          item.style.setProperty('background', '#FFD700', 'important');
          item.style.setProperty('color-adjust', 'exact');
          break;
        case 'paternity_leave':
          item.style.setProperty('background', '#FFFB00', 'important');
          item.style.setProperty('color-adjust', 'exact');
          break;
        case 'sickness':
          item.style.setProperty('background', '#FFA500', 'important');
          item.style.setProperty('color-adjust', 'exact');
          break;
        case 'accident':
          item.style.setProperty('background', '#00FFE7', 'important');
          item.style.setProperty('color-adjust', 'exact');
          break;
        case 'compensation':
          item.style.setProperty('background', '#00BFFF', 'important');
          item.style.setProperty('color-adjust', 'exact');
          break;
        case 'public_holiday':
          item.style.setProperty('background', '#00FF00', 'important');
          item.style.setProperty('color-adjust', 'exact');
          break;
        case 'reduction':
          item.style.setProperty('background', '#FF0000', 'important');
          item.style.setProperty('color-adjust', 'exact');
          break;
        case 'school':
          item.style.setProperty('background', '#EE82EE', 'important');
          item.style.setProperty('color-adjust', 'exact');
          break;
        case 'military':
          item.style.setProperty('background', '#9ACD32', 'important');
          item.style.setProperty('color-adjust', 'exact');
          break;
        default:
          var new_date = new Date(sel_date);
          new_date.setDate(new_date.getDate() + day + 1);
          if (new_date.getDay() in [0, 6]) {
            item.style.setProperty('background', '#C0C0C0', 'important');
            item.style.setProperty('color-adjust', 'exact');
          } else {
            item.style.setProperty('background', '');
            item.style.setProperty('color-adjust', '');
          }
      }
    }
    selectionChange();
  }


  var loadData = function() {
    var user_selection = document.getElementById('user_selection');
    if (user_selection) {
      load_data_from_user = Number(user_selection.value);
    } else {
      load_data_from_user = null;
    }
    var date_selected = document.getElementById('date_selected').value;
    load_data_from_year = date_selected.substr(0, 4);
    load_data_from_month = date_selected.substr(5, 2);

    var month_controls = document.getElementsByClassName('timesheet_month');
    for (var i = 0; i < month_controls.length; ++i) {
      var item = month_controls[i];
      item.style.setProperty('display', 'none');
    }
    document.getElementById('timesheet_month_' + load_data_from_month).style.setProperty('display', '');
    document.getElementById('timesheet_year').innerHTML = load_data_from_year;

    ajax.jsonRpc('/stalder_tiefbau_ag/timesheet/load', 'call', {
      'user': load_data_from_user,
      'year': load_data_from_year,
      'month': load_data_from_month,
    }).then(function (result) {
      if (result) {
        for (var i = 0; i < 31; ++i) {
          document.getElementById('tx_0_' + i.toString() + '_s').value = null;
          document.getElementById('tx_0_' + i.toString() + '_x').value = null;
        }
        for (var i = 0; i < 30; ++i) {
          document.getElementById('bt_' + i.toString() + '_d').value = null;
          document.getElementById('nr_' + i.toString() + '_a').value = null;
          for (var j = 0; j < 31; ++j) {
            document.getElementById('tx_' + i.toString() + '_' + j.toString() + '_a').value = null;
            document.getElementById('tx_' + i.toString() + '_' + j.toString() + '_v').value = null;
            document.getElementById('tx_' + i.toString() + '_' + j.toString() + '_r').value = null;
            document.getElementById('tx_' + i.toString() + '_' + j.toString() + '_m').checked = false;
          }
        }
        document.getElementById('timesheet_remark').value = null;

        var lots = result[0];
        if (lots) {
          for (var i = 0; i < lots.length; ++i) {
            var lot = lots[i];
            if (lot[0].length > 0) {
              var lot_control = document.getElementById('bt_' + i.toString() + '_d');
              lot_control.value = lot[0];
            }
            if (lot[1].length > 0) {
              var number_control = document.getElementById('nr_' + i.toString() + '_a');
              number_control.value = lot[1];
            }
            for (var j = 0; j < lot[2].length; ++j) {
              var day = lot[2][j];
              if ((day[1]) && (day[1] != 0)) {
                document.getElementById('tx_' + i.toString() + '_' + (day[0] - 1).toString() + '_a').value = day[1];
              }
              if ((day[2]) && (day[2] != 0)) {
                document.getElementById('tx_' + i.toString() + '_' + (day[0] - 1).toString() + '_v').value = day[2];
              }
              if ((day[3]) && (day[3] != 0)) {
                document.getElementById('tx_' + i.toString() + '_' + (day[0] - 1).toString() + '_r').value = day[3];
              }
              document.getElementById('tx_' + i.toString() + '_' + (day[0] - 1).toString() + '_m').checked = day[4];
            }
          }
        }

        var others = result[1];
        if (others) {
          for (var i = 0; i < others.length; ++i) {
              var other = others[i];
              document.getElementById('tx_0_' + (other[0] - 1).toString() + '_s').value = other[1];
              if ((other[2]) && (other[2] != 0)) {
                document.getElementById('tx_0_' + (other[0] - 1).toString() + '_x').value = other[2];
              }
          }
        }

        var remark = result[2];
        if (remark) {
          document.getElementById('timesheet_remark').value = remark;
        }

        var is_timesheet_admin = result[3];
        var current_month_is_closed = result[4];
        var prev_month_is_closed = result[5];

        document.getElementById('label_curr_is_closed').style.setProperty('display', 'none');
        document.getElementById('label_curr_not_closed').style.setProperty('display', 'none');
        document.getElementById('label_prev_is_closed').style.setProperty('display', 'none');
        document.getElementById('label_prev_not_closed').style.setProperty('display', 'none');
        document.getElementById('button_close_month').style.setProperty('display', 'none');

        if (current_month_is_closed) {
          document.getElementById('label_curr_is_closed').style.setProperty('display', '');
        } else {
          document.getElementById('button_close_month').style.setProperty('display', '');
        }
        if (is_timesheet_admin) {
          if (!current_month_is_closed) {
            document.getElementById('label_curr_not_closed').style.setProperty('display', '');
          }
        } else {
          if (prev_month_is_closed) {
            document.getElementById('label_prev_is_closed').style.setProperty('display', '');
          } else {
            document.getElementById('label_prev_not_closed').style.setProperty('display', '');
          }
        }

        if ((!current_month_is_closed) || (is_timesheet_admin)) {
          var text_controls = document.getElementsByClassName('timesheet_table_input_text');
          for (var i = 0; i < text_controls.length; ++i) {
            var item = text_controls[i];
            item.readOnly = false;
          }
          var checkbox_controls = document.getElementsByClassName('timesheet_table_input_checkbox');
          for (var i = 0; i < checkbox_controls.length; ++i) {
            var item = checkbox_controls[i];
            item.disabled = false;
          }
          var checkbox_controls = document.getElementsByClassName('timesheet_table_input_lookup');
          for (var i = 0; i < checkbox_controls.length; ++i) {
            var item = checkbox_controls[i];
            item.disabled = false;
          }
          document.getElementById('timesheet_remark').readOnly = false;
        }

        dateChange();
        for (var i = 0; i < 31; ++i) {
          recalcTimesheet('', 'a', i.toString());
          recalcTimesheet('', 'v', i.toString());
          recalcTimesheet('', 'r', i.toString());
          recalcTimesheet('', 'm', i.toString());
        }

        if ((current_month_is_closed) && (!is_timesheet_admin)) {
          var text_controls = document.getElementsByClassName('timesheet_table_input_text');
          for (var i = 0; i < text_controls.length; ++i) {
            var item = text_controls[i];
            item.readOnly = true;
          }
          var checkbox_controls = document.getElementsByClassName('timesheet_table_input_checkbox');
          for (var i = 0; i < checkbox_controls.length; ++i) {
            var item = checkbox_controls[i];
            item.disabled = true;
          }
          var checkbox_controls = document.getElementsByClassName('timesheet_table_input_lookup');
          for (var i = 0; i < checkbox_controls.length; ++i) {
            var item = checkbox_controls[i];
            item.disabled = true;
          }
          document.getElementById('timesheet_remark').readOnly = true;
        }
      }
    });
  }


  var saveData = function(reloadData, showMessage) {
    var lots = [];
    var lot_description_controls = document.getElementsByClassName('timesheet_lot_description_input');
    for (var i = 0; i < lot_description_controls.length; ++i) {
      var lot_description_control = lot_description_controls[i];
      if (lot_description_control.value.length > 0) {
        var row = ''
        if (lot_description_control.id.substr(4, 1) == '_') {
          row = lot_description_control.id.substr(3, 1);
        } else {
          row = lot_description_control.id.substr(3, 2);
        }
        var number_control = document.getElementById('nr_' + row + '_a');

        var days = [];
        for (var j = 0; j < 31; ++j) {
          var work_duration = document.getElementById('tx_' + row + '_' + j.toString() + '_a').value;
          var prepare_duration = document.getElementById('tx_' + row + '_' + j.toString() + '_v').value;
          var travel_duration = document.getElementById('tx_' + row + '_' + j.toString() + '_r').value;
          var food_charge = document.getElementById('tx_' + row + '_' + j.toString() + '_m').checked;

          var day = [work_duration, prepare_duration, travel_duration, food_charge];
          days.push(day);
        }

        var lot = [i, lot_description_control.value, number_control.value, days];
        lots.push(lot);
      }
    }

    var others = [];
    for (var i = 0; i < 31; ++i) {
      var other_type = document.getElementById('tx_0_' + i.toString() + '_s').value;
      var duration = document.getElementById('tx_0_' + i.toString() + '_x').value;
      if ((other_type) && (other_type == 'work')) {
        other_type = null;
        duration = null;
      }

      var other = [other_type, duration];
      others.push(other);
    }

    var remark = document.getElementById('timesheet_remark').value;


    save_done = false;
    ajax.jsonRpc('/stalder_tiefbau_ag/timesheet/save', 'call', {
      'user': load_data_from_user,
      'year': load_data_from_year,
      'month': load_data_from_month,
      'lots': lots,
      'others': others,
      'remark': remark,
    }).then(function (result) {
      if (result.length == 0) {
        if ((reloadData) && (do_reload)) {
          loadData();
        }
        save_done = true;
      } else {
        if (showMessage) {
          loadData();
          window.alert(result[3]);
          if ((result[0] >= 0) && (result[1] >= 0) && (result[2].length > 0)) {
            document.getElementById('tx_' + result[0].toString() + '_' + result[1].toString() + '_' + result[2]).focus();
          } else if ((result[1] >= 0) && (result[2].length > 0)) {
            document.getElementById('tx_0_' + result[1].toString() + '_' + result[2]).focus();
          }
        }
      }
    }).fail(function () {
      if (showMessage) {
        window.alert('Unknown error!');
      }
    });
  }

  var closeMonth = function() {
    ajax.jsonRpc('/stalder_tiefbau_ag/timesheet/close', 'call', {
      'user': load_data_from_user,
      'year': load_data_from_year,
      'month': load_data_from_month,
    }).then(function (result) {
      loadData();
    }).fail(function () {
      window.alert('Unknown error!');
    });
  }

  $(document).ready(function() {
    var timesheet_container = document.getElementById('timesheet_container');
    if (timesheet_container != null) {
      document.getElementById('date_selected').value = new Date().toISOString().substr(0, 10);

      loadData();
    }
  });


  $(document).on('change', '#date_selected', function(event) {
    saveData(true, false);
    save_done = true;
  });


  $(document).on('change', '#user_selection', function(event) {
    saveData(true, false);
    save_done = true;
  });


  $(document).on('change', '#time_selection', function(event) {
    selectionChange();
  });


  $(document).on('click', '#button_prev_month', function(event) {
    if (save_done) {
      saveData(false, false);

      var calc_month = Number(document.getElementById('date_selected').value.substr(5, 2));
      var calc_year = Number(document.getElementById('date_selected').value.substr(0, 4));
      if (calc_month == 1) {
        var calc_date = (calc_year - 1).toString() + '-12-01';
      } else if (calc_month <= 10) {
        var calc_date = (calc_year - 0).toString() + '-0' + (calc_month - 1).toString() + '-01';
      } else {
        var calc_date = (calc_year - 0).toString() + '-' + (calc_month - 1).toString() + '-01';
      }
      document.getElementById('date_selected').value = calc_date;

      loadData();
    }
    save_done = true;
  });


  $(document).on('click', '#button_prev_week', function(event) {
    if (save_done) {
      saveData(false, false);

      var prev_last_month = new Date(document.getElementById('date_selected').value.substr(0, 8) + '01');
      prev_last_month.setDate(prev_last_month.getDate() - 1);

      var prev_sunday = new Date(document.getElementById('date_selected').value.substr(0, 10));
      if (prev_sunday.getDay() == 0) {
        prev_sunday.setDate(prev_sunday.getDate() - 7);
      } else {
        prev_sunday.setDate(prev_sunday.getDate() - prev_sunday.getDay());
      }

      if (prev_sunday.getMonth() == prev_last_month.getMonth()) {
        document.getElementById('date_selected').value = prev_last_month.toISOString().substr(0, 10);
      } else {
        document.getElementById('date_selected').value = prev_sunday.toISOString().substr(0, 10);
      }

      loadData();
    }
    save_done = true;
  });


  $(document).on('click', '#button_next_week', function(event) {
    if (save_done) {
      saveData(false, false);

      var next_first_month = new Date(document.getElementById('date_selected').value.substr(0, 8) + '01');
      next_first_month.setMonth(next_first_month.getMonth() + 1);

      var next_monday = new Date(document.getElementById('date_selected').value.substr(0, 10));
      if (next_monday.getDay() == 0) {
        next_monday.setDate(next_monday.getDate() - 6);
      } else {
        next_monday.setDate(next_monday.getDate() - (next_monday.getDay() - 1));
      }
      next_monday.setDate(next_monday.getDate() + 7);

      if (next_monday.getMonth() == next_first_month.getMonth()) {
        document.getElementById('date_selected').value = next_first_month.toISOString().substr(0, 10);
      } else {
        document.getElementById('date_selected').value = next_monday.toISOString().substr(0, 10);
      }

      loadData();
    }
    save_done = true;
  });


  $(document).on('click', '#button_next_month', function(event) {
    if (save_done) {
      saveData(false, false);

      var calc_month = Number(document.getElementById('date_selected').value.substr(5, 2));
      var calc_year = Number(document.getElementById('date_selected').value.substr(0, 4));
      if (calc_month == 12) {
        var calc_date = (calc_year + 1).toString() + '-01-01';
      } else if (calc_month < 9) {
        var calc_date = (calc_year + 0).toString() + '-0' + (calc_month + 1).toString() + '-01';
      } else {
        var calc_date = (calc_year + 0).toString() + '-' + (calc_month + 1).toString() + '-01';
      }
      document.getElementById('date_selected').value = calc_date;

      loadData();
    }
    save_done = true;
  });


  $(document).on('click', '#button_close_month', function(event) {
    if (save_done) {
      closeMonth();
    }
    save_done = true;
  });


  $(document).on('change', '.timesheet_other_selection', function(event) {
    dateChange();
    saveData(false, true);
    save_done = true;
  });


  $(document).on('change', '.timesheet_lot_description_input', function(event) {
    selectionChange();
    saveData(false, true);
    save_done = true;
  });


  $(document).on('change', '.timesheet_lot_number_input', function(event) {
    saveData(false, true);
    save_done = true;
  });


  var saveDateWithoutReload = function() {
    saveData(true, true)
  }

  var saveDateWithReload = function() {
    if (save_done) {
      saveData(true, true)
    }
    save_done = true;
    do_reload = true;
  }

  $(document).on('change', '.timesheet_time_text_change', function(event) {
    setTimeout(saveDateWithoutReload, 100);
  });

  $(document).on('click', '.timesheet_time_text_click', function(event) {
    do_reload = false;
    setTimeout(saveDateWithReload, 100);
  });


  $(document).on('change', '#timesheet_remark', function(event) {
    saveData(false, true);
    save_done = true;
  });

});