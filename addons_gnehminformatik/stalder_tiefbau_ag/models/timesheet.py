# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime
from odoo.tools.misc import DEFAULT_SERVER_DATE_FORMAT


def attrs(**kwargs):
    return {'data-oe-%s' % key: str(value) for key, value in kwargs.items()}

other_types = [
    ('holiday', 'Holiday'),
    ('paternity_leave', 'Paternity Leave'),
    ('sickness', 'Sickness'),
    ('accident', 'Accident'),
    ('compensation', 'Compensation'),
    ('public_holiday', 'Public Holiday'),
    ('reduction', 'Reduction'),
    ('school', 'School/Workshop'),
    ('military', 'Military'),
]


class StalderTiefbauAGTimesheetMonth(models.Model):

    _name = 'stalder_tiefbau_ag.timesheet_month'
    _description = 'Timesheet: Month'
    _order = 'user_id, year, month'

    name = fields.Char(compute='compute_name')

    user_id = fields.Many2one('res.users', string='User', ondelete='cascade', required=True, index=True)
    year = fields.Integer(string='Year', required=True, index=True)
    month = fields.Integer(string='Month', required=True, index=True)
    remark = fields.Text(string='Remark')
    closed = fields.Boolean(string='Closed', default=False)

    timesheet_lot_ids = fields.One2many('stalder_tiefbau_ag.timesheet_lot', 'timesheet_month_id', string='Lots')
    timesheet_other_ids = fields.One2many('stalder_tiefbau_ag.timesheet_other', 'timesheet_month_id', string='Other')

    @api.one
    def compute_name(self):
        self.name = '{0} ({1}-{2})'.format(self.user_id.name, str(self.year), str(self.month))


class StalderTiefbauAGTimesheetLot(models.Model):

    _name = 'stalder_tiefbau_ag.timesheet_lot'
    _description = 'Timesheet: Lot'
    _order = 'timesheet_month_id, sequence'

    name = fields.Char(compute='compute_name')

    timesheet_month_id = fields.Many2one('stalder_tiefbau_ag.timesheet_month', string='Month', ondelete='cascade', required=True, index=True)
    user_id = fields.Many2one('res.users', string='User', compute='compute_month', store=True)
    year = fields.Integer(string='Year', compute='compute_month', store=True)
    month = fields.Integer(string='Month', compute='compute_month', store=True)

    sequence = fields.Integer(string='Sequence', required=True, default=0)
    description = fields.Char(string='Description')
    number = fields.Char(string='Number')

    work_duration = fields.Float(string='Work Duration', digits=(10, 2), compute='compute_duration', store=True)
    prepare_duration = fields.Float(string='Prepare Duration', digits=(10, 2), compute='compute_duration', store=True)
    total_duration = fields.Float(string='Total Duration', digits=(10, 2), compute='compute_duration', store=True)
    travel_duration = fields.Float(string='Travel Duration', digits=(10, 2), compute='compute_duration', store=True)
    food_charge = fields.Integer(string='Food Charge', compute='compute_duration', store=True)

    show_prepare_duration = fields.Boolean(string='Show Prepare Duration', compute='compute_show_duration', store=True)
    show_travel_duration = fields.Boolean(string='Show Travel Duration',  compute='compute_show_duration', store=True)

    timesheet_day_ids = fields.One2many('stalder_tiefbau_ag.timesheet_day', 'timesheet_lot_id', string='Days')

    @api.one
    def compute_name(self):
        self.name = '{0}: {1}. {2} ({3})'.format(self.timesheet_month_id.name, str(self.sequence + 1), self.description or '', self.number or '')

    @api.one
    @api.depends('timesheet_month_id','timesheet_month_id.year','timesheet_month_id.month')
    def compute_month(self):
        self.user_id = self.timesheet_month_id.user_id
        self.year = self.timesheet_month_id.year
        self.month = self.timesheet_month_id.month

    @api.multi
    @api.depends('timesheet_day_ids.work_duration', 'timesheet_day_ids.prepare_duration', 'timesheet_day_ids.travel_duration', 'timesheet_day_ids.food_charge')
    def compute_duration(self):
        for record in self:
            work_duration = 0.0
            prepare_duration = 0.0
            travel_duration = 0
            food_charge = 0
            for line in record.timesheet_day_ids:
                work_duration += line.work_duration
                prepare_duration += line.prepare_duration
                travel_duration += line.travel_duration
                if line.food_charge:
                    food_charge += 1
            record['work_duration'] = work_duration
            record['prepare_duration'] = prepare_duration
            record['total_duration'] = work_duration + prepare_duration
            record['travel_duration'] = travel_duration / 60
            record['food_charge'] = food_charge

    @api.multi
    @api.depends('year', 'month')
    def compute_show_duration(self):
        for record in self:
            if (    record.year
                and (  (record.year < 2019)
                    or ((record.year == 2019) and record.month and (record.month < 4))
                )
            ):
                record['show_prepare_duration'] = True
                record['show_travel_duration'] = False
            else:
                record['show_prepare_duration'] = False
                record['show_travel_duration'] = True


class StalderTiefbauAGTimesheetDay(models.Model):

    _name = 'stalder_tiefbau_ag.timesheet_day'
    _description = 'Timesheet: Day'
    _order = 'timesheet_lot_id, day'

    name = fields.Char(compute='compute_name')

    timesheet_lot_id = fields.Many2one('stalder_tiefbau_ag.timesheet_lot', string='Lot', ondelete='cascade', required=True, index=True)
    day = fields.Integer(string='Day', required=True, index=True)
    work_duration = fields.Float(string='Work Duration', digits=(10, 2))
    prepare_duration = fields.Float(string='Prepare Duration', digits=(10, 2))
    travel_duration = fields.Integer(string='Travel Duration in Minutes')
    food_charge = fields.Boolean(string='Food Charge')

    show_prepare_duration = fields.Boolean(string='Show Prepare Duration', compute='compute_show_duration', store=True)
    show_travel_duration = fields.Boolean(string='Show Travel Duration',  compute='compute_show_duration', store=True)

    @api.one
    def compute_name(self):
        self.name = '{0}: {1}'.format(self.timesheet_lot_id.name, str(self.day))

    @api.multi
    @api.depends('timesheet_lot_id', 'timesheet_lot_id.year', 'timesheet_lot_id.month')
    def compute_show_duration(self):
        for record in self:
            if (    record.timesheet_lot_id
                and record.timesheet_lot_id.year
                and (  (record.timesheet_lot_id.year < 2019)
                    or ((record.timesheet_lot_id.year == 2019) and record.timesheet_lot_id.month and (record.timesheet_lot_id.month < 4))
                )
            ):
                record['show_prepare_duration'] = True
                record['show_travel_duration'] = False
            else:
                record['show_prepare_duration'] = False
                record['show_travel_duration'] = True


class StalderTiefbauAGTimesheetOther(models.Model):

    _name = 'stalder_tiefbau_ag.timesheet_other'
    _description = 'Timesheet: Other'
    _order = 'timesheet_month_id, day'

    name = fields.Char(compute='compute_name')

    timesheet_month_id = fields.Many2one('stalder_tiefbau_ag.timesheet_month', string='Month', ondelete='cascade', required=True, index=True)
    day = fields.Integer(string='Day', required=True, index=True)
    type = fields.Selection(other_types, required=True, default='holiday')
    duration = fields.Float(string='Duration', digits=(10, 2))

    @api.one
    def compute_name(self):
        self.name = '{0}: {1} ({2})'.format(self.timesheet_month_id.name, str(self.day), dict(other_types)[self.type])


class StalderTiefbauAGTimesheetTemplate(models.Model):

    _name = 'stalder_tiefbau_ag.timesheet_template'
    _description = 'Timesheet: Template'
    _order = 'date, type'

    name = fields.Char(compute='compute_name')

    date = fields.Date(string='Date', required=True, index=True)
    type = fields.Selection(other_types, required=True, default='public_holiday')
    duration = fields.Float(string='Duration', digits=(10, 2))

    user_ids = fields.Many2many('res.users', string='Valid for')

    @api.one
    def compute_name(self):
        lang_code = self.env.context.get('lang') or 'en_US'
        lang = self.env['res.lang']
        lang_id = lang._lang_get(lang_code)
        self.name = '{0}: {1}'.format(datetime.strftime(datetime.strptime(self.date, DEFAULT_SERVER_DATE_FORMAT), lang_id.date_format), dict(other_types)[self.type])
