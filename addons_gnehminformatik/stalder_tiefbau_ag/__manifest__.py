# -*- coding: utf-8 -*-
{
    'name': "Stalder Tiefbau AG",

    'summary': """
        Zeitrapport für die Mitarbeiter der Stalder Tiefbau AG
    """,

    'description': """
        Beinhaltet eine Website zur Erfassung der Arbeitszeiten inkl. alle nötigen Programmerweiterungen
    """,

    'author': "Gnehm Informatik GmbH",
    'website': "http://www.gnehm-informatik.ch",

    'application': True,
    'category': 'Specific Industry Applications',
    'version': '0.1',

    'depends': [
        'base',
        'in4tools_backup2sftp',
        'l10n_ch_zip',
        'partner_firstname',
        'website',
    ],

    'data': [
        'views/backend_timesheet.xml',
        'views/website_timesheet.xml',
        'security/ir.model.access.csv',
    ],
}